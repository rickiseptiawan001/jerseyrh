import FIREBASE from '../config/FIREBASE';
import { dispatchError, dispatchLoading, dispatchSuccess } from '../utils';

// Variable GET_LIST_LIGA
export const GET_LIST_LIGA = 'GET_LIST_LIGA';

// Variable GET_DETAIL_LIGA
export const GET_DETAIL_LIGA = 'GET_DETAIL_LIGA';

// Function getListLiga
export const getListLiga = () => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_LIGA);

    FIREBASE.database()
      .ref('ligas')
      .once('value', querySnapshot => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_LIGA, data);
      })
      // Error
      .catch(error => {
        dispatchError(dispatch, GET_LIST_LIGA, error);
        alert(error);
      });
  };
};

// Function getDetailLiga
export const getDetailLiga = id => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_DETAIL_LIGA);

    FIREBASE.database()
      .ref('ligas/' + id)
      .once('value', querySnapshot => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_DETAIL_LIGA, data);
      })
      // Error
      .catch(error => {
        dispatchError(dispatch, GET_DETAIL_LIGA, error);
        alert(error);
      });
  };
};
