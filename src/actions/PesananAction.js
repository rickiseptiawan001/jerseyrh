import FIREBASE from '../config/FIREBASE';
import { dispatchError, dispatchLoading, dispatchSuccess } from '../utils';

// Variable UPDATE_PESANAN
export const UPDATE_PESANAN = 'UPDATE_PESANAN';

// Arrow Function updatePesanan
export const updatePesanan = params => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, UPDATE_PESANAN);

    // Get uid user
    const uid = params.order_id.split('-')[2];
    // --------------------------------------------------------------------------------------------
    //                              [0]                [1]                            [2]
    // [2] maksudnya <- order_id: "ORDER-" + this.state.currentDate + "-" + this.state.profile.uid
    // --------------------------------------------------------------------------------------------

    // Get keranjang by uid user
    FIREBASE.database()
      .ref('keranjangs/' + uid)
      .once('value', querySnapshot => {
        if (querySnapshot.val()) {
          // Ambil data keranjang
          const data = querySnapshot.val();

          // Menambahkan data baru ke keranjang
          const dataBaru = { ...data };
          dataBaru.ongkir = params.ongkir;
          dataBaru.estimasi = params.estimasi;
          dataBaru.url = params.url;
          dataBaru.order_id = params.order_id;
          dataBaru.status = 'pending';

          // Hapus data keranjang
          FIREBASE.database()
            .ref('keranjangs/' + uid)
            .remove()
            .then(response => {
              // Buat table histories
              FIREBASE.database()
                .ref('histories')
                .child(params.order_id)
                .set(dataBaru)
                .then(response => {
                  // Sukses
                  dispatchSuccess(
                    dispatch,
                    UPDATE_PESANAN,
                    response ? response : [],
                  );
                })
                .catch(error => {
                  // Error
                  dispatchError(dispatch, UPDATE_PESANAN, error);
                  alert(error);
                });
            })
            .catch(error => {
              // Error
              dispatchError(dispatch, UPDATE_PESANAN, error);
              alert(error);
            });
        }
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, UPDATE_PESANAN, error);
        alert(error);
      });
  };
};
