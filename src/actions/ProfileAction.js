import FIREBASE from '../config/FIREBASE';
import { storeData } from '../utils';
import { dispatchError, dispatchLoading, dispatchSuccess } from '../utils';
import SweetAlert from 'react-native-sweet-alert';

// Variable UPDATE_PROFILE
export const UPDATE_PROFILE = 'UPDATE_PROFILE';

// Variable CHANGE_PASSWORD
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';

// Function updateProile
export const updateProfile = data => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, UPDATE_PROFILE);

    // Object dataBaru
    const dataBaru = {
      uid: data.uid,
      nama: data.nama,
      alamat: data.alamat,
      nohp: data.nohp,
      kota: data.kota,
      provinsi: data.provinsi,
      email: data.email,
      status: 'user',
      avatar: data.updateAvatar ? data.avatarForDB : data.avatarLama,
    };

    // Update
    FIREBASE.database()
      .ref('users/' + dataBaru.uid)
      .update(dataBaru)
      .then(response => {
        // Sukses
        dispatchSuccess(dispatch, UPDATE_PROFILE, response ? response : []);

        // Local storage (Async Storage)
        storeData('user', dataBaru);
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, UPDATE_PROFILE, error.message);

        // Tampilkan pesan error
        alert(error.message);
      });
  };
};

// Function changePassword
export const changePassword = data => {
  return dispatch => {
    dispatchLoading(dispatch, CHANGE_PASSWORD);

    // Cek dulu apakah benar email & password lama yang diketikan user sesuai dengan yang di firebase
    FIREBASE.auth()
      .signInWithEmailAndPassword(data.email, data.password)
      .then(response => {
        // Jika sukses maka update password
        const user = FIREBASE.auth().currentUser;

        user
          .updatePassword(data.newPassword)
          .then(() => {
            // Update successful.
            dispatchSuccess(dispatch, CHANGE_PASSWORD, 'Sukses ganti password');
          })
          .catch(error => {
            dispatchError(dispatch, CHANGE_PASSWORD, error);
            alert(error);
          });
      })
      .catch(error => {
        dispatchError(dispatch, CHANGE_PASSWORD, error.message);

        SweetAlert.showAlertWithOptions({
          title: 'Error!',
          subTitle: 'Password lama salah!',
          confirmButtonTitle: 'OK',
          style: 'error',
        });
      });
  };
};
