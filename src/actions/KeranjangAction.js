import FIREBASE from '../config/FIREBASE';
import { dispatchError, dispatchLoading, dispatchSuccess } from '../utils';

// Variable MASUK_KERANJANG
export const MASUK_KERANJANG = 'MASUK_KERANJANG';

// Variable GET_LIST_KERANJANG
export const GET_LIST_KERANJANG = 'GET_LIST_KERANJANG';

// Variable HAPUS_KERANJANG
export const HAPUS_KERANJANG = 'HAPUS_KERANJANG';

// Function masukKeranjang
export const masukKeranjang = data => {
  return dispatch => {
    dispatchLoading(dispatch, MASUK_KERANJANG);

    // Cek apakah data keranjang user tersebut sudah ada atau tidak
    FIREBASE.database()
      .ref('keranjangs/' + data.uid)
      .once('value', querySnapshot => {
        console.log(
          'Cek apakah data keranjang user tersebut sudah ada atau tidak',
          querySnapshot.val(),
        );

        /* Cek querySnapshot.val() apakah ada datanya ?, jika ada jalankan code if, 
                jika tidak ada jalankan code else*/
        if (querySnapshot.val()) {
          // Update keranjang utama
          const keranjangUtama = querySnapshot.val();
          const hargaBaru = parseInt(data.jumlah) * parseInt(data.jersey.harga);
          const beratBaru =
            parseInt(data.jumlah) * parseFloat(data.jersey.berat);

          // Simpan ke firebase
          FIREBASE.database()
            .ref('keranjangs')
            .child(data.uid)
            .update({
              totalBerat: keranjangUtama.totalBerat + beratBaru,
              totalHarga: keranjangUtama.totalHarga + hargaBaru,
            })
            .then(response => {
              // Simpan ke keranjang detail
              dispatch(masukKeranjangDetail(data));
            })
            .catch(error => {
              dispatchError(dispatch, MASUK_KERANJANG, error);
              alert(error);
            });
        } else {
          // Simpan keranjang utama
          const keranjangUtama = {
            user: data.uid,
            tanggal: new Date().toDateString(),
            // harga dikali jumlah
            totalHarga: parseInt(data.jumlah) * parseInt(data.jersey.harga),
            totalBerat: parseFloat(data.jumlah) * parseFloat(data.jersey.berat),
          };

          // Simpan ke firebase
          FIREBASE.database()
            .ref('keranjangs')
            .child(data.uid)
            .set(keranjangUtama)
            .then(response => {
              console.log('Simpan keranjang utama', response);
              // Simpan ke keranjang detail
              dispatch(masukKeranjangDetail(data));
            })
            .catch(error => {
              dispatchError(dispatch, MASUK_KERANJANG, error);
              alert(error);
            });
        }
      })
      .catch(error => {
        dispatchError(dispatch, MASUK_KERANJANG, error);
        alert(error);
      });
  };
};

// Function masukKeranjangDetail
export const masukKeranjangDetail = data => {
  return dispatch => {
    const pesanans = {
      product: data.jersey,
      jumlahPesan: data.jumlah,
      totalHarga: parseInt(data.jumlah) * parseInt(data.jersey.harga),
      totalBerat: parseInt(data.jumlah) * parseFloat(data.jersey.berat),
      ukuran: data.ukuran,
    };

    FIREBASE.database()
      .ref('keranjangs/' + data.uid)
      .child('pesanans')
      .push(pesanans)
      .then(response => {
        console.log('Simpan keranjang detail', response);
        dispatchSuccess(dispatch, MASUK_KERANJANG, response ? response : []);
      })
      .catch(error => {
        dispatchError(dispatch, MASUK_KERANJANG, error);
        alert(error);
      });
  };
};

// Function getListKeranjang
export const getListKeranjang = id => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_KERANJANG);

    FIREBASE.database()
      .ref('keranjangs/' + id)
      .once('value', querySnapshot => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_KERANJANG, data);
      })
      // Error
      .catch(error => {
        dispatchError(dispatch, GET_LIST_KERANJANG, error);
        alert(error);
      });
  };
};

// Function deleteKeranjang
export const deleteKeranjang = (id, keranjang, keranjangUtama) => {
  return dispatch => {
    dispatchLoading(dispatch, HAPUS_KERANJANG);

    const totalHargaBaru = keranjangUtama.totalHarga - keranjang.totalHarga;
    const totalBeratBaru = keranjangUtama.totalBerat - keranjang.totalBerat;

    if (totalHargaBaru === 0) {
      // Hapus keranjangUtama & keranjangDetail
      FIREBASE.database()
        .ref('keranjangs')
        .child(keranjangUtama.user)
        .remove()
        .then(response => {
          dispatchSuccess(
            dispatch,
            HAPUS_KERANJANG,
            'Keranjang sukses dihapus',
          );
        })
        // Error
        .catch(error => {
          dispatchError(dispatch, HAPUS_KERANJANG, error);
          alert(error);
        });
    } else {
      // Update keranjangUtama
      FIREBASE.database()
        .ref('keranjangs')
        .child(keranjangUtama.user)
        .update({
          totalBerat: totalBeratBaru,
          totalHarga: totalHargaBaru,
        })
        .then(response => {
          // Hapus keranjangDetail
          dispatch(deleteKeranjangDetail(id, keranjangUtama));
        })
        .catch(error => {
          dispatchError(dispatch, HAPUS_KERANJANG, error);
          alert(error);
        });
    }
  };
};

// Function deleteKeranjangDetail
export const deleteKeranjangDetail = (id, keranjangUtama) => {
  return dispatch => {
    FIREBASE.database()
      .ref('keranjangs/' + keranjangUtama.user)
      .child('pesanans')
      .child(id)
      .remove()
      .then(response => {
        dispatchSuccess(dispatch, HAPUS_KERANJANG, 'Keranjang sukses dihapus');
      })
      .catch(error => {
        dispatchError(dispatch, HAPUS_KERANJANG, error);
        alert(error);
      });
  };
};
