import FIREBASE from '../config/FIREBASE';
import {
  storeData,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
} from '../utils';
import SweetAlert from 'react-native-sweet-alert';

export const REGISTER_USER = 'REGISTER_USER';
export const LOGIN_USER = 'LOGIN_USER';

// Function registerUser
export const registerUser = (data, password) => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, REGISTER_USER);

    FIREBASE.auth()
      .createUserWithEmailAndPassword(data.email, password)
      .then(success => {
        // Ambil UID, buat dataBaru (data+uid)
        const dataBaru = {
          ...data,
          uid: success.user.uid,
        };

        // Simpan ke realtime database firebase
        FIREBASE.database()
          .ref('users/' + success.user.uid)
          .set(dataBaru);

        // Sukses
        dispatchSuccess(dispatch, REGISTER_USER, dataBaru);

        // Local storage (Async Storage)
        storeData('user', dataBaru);
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, REGISTER_USER, error.message);

        alert(error.message);
      });
  };
};

// Function loginUser
export const loginUser = (email, password) => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, LOGIN_USER);

    FIREBASE.auth()
      .signInWithEmailAndPassword(email, password)
      .then(success => {
        // Berhasil login
        FIREBASE.database()
          .ref('/users/' + success.user.uid)
          .once('value')
          .then(resDB => {
            if (resDB.val()) {
              // Sukses
              dispatchSuccess(dispatch, LOGIN_USER, resDB.val());

              //Local storage (Async Storage)
              storeData('user', resDB.val());
            } else {
              // Error
              dispatch({
                type: LOGIN_USER,
                payload: {
                  loading: false,
                  data: false,
                  errorMessage: 'Data User tidak ada',
                },
              });

              // Tampilkan pesan
              alert('Data User tidak ada');
            }
          });
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, LOGIN_USER, error.message);

        // Tampilkan
        SweetAlert.showAlertWithOptions({
          title: 'Error!',
          subTitle: 'Email atau password anda salah',
          confirmButtonTitle: 'OK',
          style: 'error',
        });
      });
  };
};
