import axios from 'axios';
import {
  API_TIMEOUT,
  URL_MIDTRANS,
  HEADER_MIDTRANS,
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
} from '../utils';

// Variable SNAP_TRANSACTION
export const SNAP_TRANSACTIONS = "SNAP_TRANSACTIONS";

// Arrow Function snapTransactions
export const snapTransactions = (data) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, SNAP_TRANSACTIONS);

    // Axios
    axios({
      method: "POST",
      url: URL_MIDTRANS + "transactions",
      headers: HEADER_MIDTRANS,
      data: data,
      timeout: API_TIMEOUT
      // Jika sukses
    }).then(function (response) {
      // Sukses
      dispatchSuccess(dispatch, SNAP_TRANSACTIONS, response.data)
      // Jika error
    }).catch(function (error) {
      // Error
      dispatchError(dispatch, SNAP_TRANSACTIONS, error)
      alert(error)
    })
  }
}
