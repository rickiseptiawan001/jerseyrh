import FIREBASE from '../config/FIREBASE';
import { dispatchError, dispatchLoading, dispatchSuccess } from '../utils';

// Variable GET_LIST_JERSEY
export const GET_LIST_JERSEY = 'GET_LIST_JERSEY';

// Variable GET_LIST_JERSEY_BY_LIGA
export const GET_LIST_JERSEY_BY_LIGA = 'GET_LIST_JERSEY_BY_LIGA';

// Variable SAVE_KEYWORD_JERSEY
export const SAVE_KEYWORD_JERSEY = 'SAVE_KEYWORD_JERSEY';

// Function getListJersey
export const getListJersey = (idLiga, keyword) => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_JERSEY);
    if (idLiga) {
      FIREBASE.database()
        // Nama table di firebase
        .ref('jerseys')
        // Berdasarkan liga
        .orderByChild('liga')
        .equalTo(idLiga)
        .once('value', querySnapshot => {
          // Hasil
          let data = querySnapshot.val();

          // Sukses
          dispatchSuccess(dispatch, GET_LIST_JERSEY, data);
        })
        // Error
        .catch(error => {
          dispatchError(dispatch, GET_LIST_JERSEY, error);
          alert(error);
        });
    } else if (keyword) {
      FIREBASE.database()
        // Nama table di firebase
        .ref('jerseys')
        // Berdasarkan klub
        .orderByChild('klub')
        .equalTo(keyword.toUpperCase())
        .once('value', querySnapshot => {
          //Hasil
          let data = querySnapshot.val();

          dispatchSuccess(dispatch, GET_LIST_JERSEY, data);
        })
        .catch(error => {
          dispatchError(dispatch, GET_LIST_JERSEY, error);
          alert(error);
        });
    } else {
      FIREBASE.database()
        .ref('jerseys')
        .once('value', querySnapshot => {
          // Hasil
          let data = querySnapshot.val();

          // Sukses
          dispatchSuccess(dispatch, GET_LIST_JERSEY, data);
        })
        // Error
        .catch(error => {
          dispatchError(dispatch, GET_LIST_JERSEY, error);
          alert(error);
        });
    }
  };
};

// Function getListJersey
export const limitJersey = () => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_JERSEY);

    FIREBASE.database()
      .ref('jerseys')
      // Dibatasi hanya menampilkan 4 data terbaru
      .limitToLast(4)
      .once('value', querySnapshot => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_JERSEY, data);
      })
      // Error
      .catch(error => {
        dispatchError(dispatch, GET_LIST_JERSEY, error);
        alert(error);
      });
  };
};

// Function getJerseyByLiga
export const getJerseyByLiga = (id, namaLiga) => ({
  type: GET_LIST_JERSEY_BY_LIGA,
  payload: {
    idLiga: id,
    namaLiga: namaLiga,
  },
});

// Function saveKeywordJersey
export const saveKeywordJersey = search => ({
  type: SAVE_KEYWORD_JERSEY,
  payload: {
    data: search,
  },
});
