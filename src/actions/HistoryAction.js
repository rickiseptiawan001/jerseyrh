import axios from 'axios';
import SweetAlert from 'react-native-sweet-alert';
import FIREBASE from '../config/FIREBASE';

import {
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  API_TIMEOUT,
  URL_MIDTRANS_STATUS,
  HEADER_MIDTRANS,
} from '../utils';

// Variable GET_LIST_HISTORY
export const GET_LIST_HISTORY = 'GET_LIST_HISTORY';

// Variable UPDATE_STATUS
export const UPDATE_STATUS = 'UPDATE_STATUS';

// Arrow Function getListHistory
export const getListHistory = uid => {
  return dispatch => {
    dispatchLoading(dispatch, GET_LIST_HISTORY);

    FIREBASE.database()
      .ref('histories')
      .orderByChild('user')
      .equalTo(uid)
      .once('value', querySnapshot => {
        let data = querySnapshot.val();
        dispatchSuccess(dispatch, GET_LIST_HISTORY, data);
      })
      .catch(error => {
        dispatchError(dispatch, GET_LIST_HISTORY, error.message);
        SweetAlert.showAlertWithOptions({
          title: 'Error!',
          subTitle: error.message,
          confirmButtonTitle: 'OK',
          style: 'error',
        });

        console.log(error.message);
      });
  };
};

// Arrow Function updateStatus
export const updateStatus = order_id => {
  return dispatch => {
    console.log('order_id pesanan', order_id);
    dispatchLoading(dispatch, UPDATE_STATUS);

    axios({
      method: 'GET',
      url: URL_MIDTRANS_STATUS + `${order_id}/status`,
      timeout: API_TIMEOUT,
      headers: HEADER_MIDTRANS,
    })
      .then(response => {
        const status =
          response.data.transaction_status === 'settlement' ||
            response.data.transaction_status === 'capture'
            ? 'lunas'
            : response.data.transaction_status
              ? response.data.transaction_status
              : 'pending';

        // Update status di table histories jadi 'lunas'
        FIREBASE.database()
          .ref('histories')
          .child(order_id)
          .update({
            status: status,
          })
          .then(response => {
            dispatchSuccess(dispatch, UPDATE_STATUS, response ? response : []);
          })
          .catch(error => {
            dispatchError(dispatch, UPDATE_STATUS, error.message);
            SweetAlert.showAlertWithOptions({
              title: 'Error!',
              subTitle: 'pesan error 1 ' + error.message,
              confirmButtonTitle: 'OK',
              style: 'error',
            });
            console.log(error.message);
          });
      })
      .catch(error => {
        dispatchError(dispatch, UPDATE_STATUS, error.response);
        SweetAlert.showAlertWithOptions({
          title: 'Error!',
          subTitle: 'pesan error 2 ' + error.response,
          confirmButtonTitle: 'OK',
          style: 'error',
        });

        console.log(error.response);
      });
  };
};
