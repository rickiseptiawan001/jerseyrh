import axios from 'axios';
import {
  API_HEADER_RAJAONGKIR,
  API_RAJAONGKIR,
  API_TIMEOUT,
  API_HEADER_RAJAONGKIR_COST,
  ORIGIN_CITY,
} from '../utils/constant';
import { dispatchError, dispatchLoading, dispatchSuccess } from '../utils';

// Variable GET_PROVINSI
export const GET_PROVINSI = 'GET_PROVINSI';

// Variable GET_KOTA
export const GET_KOTA = 'GET_KOTA';

// Variable GET_KOTA_DETAIL
export const GET_KOTA_DETAIL = 'GET_KOTA_DETAIL';

// Variable POST_ONGKIR
export const POST_ONGKIR = 'POST_ONGKIR';

// Function getProvinsiList
export const getProvinsiList = () => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_PROVINSI);

    // Axios
    axios({
      method: 'get',
      url: API_RAJAONGKIR + 'province',
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then(response => {
        if (response.status !== 200) {
          // Error
          dispatchError(dispatch, GET_PROVINSI, response);
        } else {
          // Sukses
          dispatchSuccess(
            dispatch,
            GET_PROVINSI,
            response.data ? response.data.rajaongkir.results : [],
          );
        }
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, GET_PROVINSI, error);

        // Tampilkan pesan error
        alert(error);
      });
  };
};

// Function getKotaList
export const getKotaList = provinsi_id => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_KOTA);

    // Axios
    axios({
      method: 'get',
      url: API_RAJAONGKIR + 'city?province=' + provinsi_id,
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then(response => {
        if (response.status !== 200) {
          // Error
          dispatchError(dispatch, GET_KOTA, response);
        } else {
          // Sukses
          dispatchSuccess(
            dispatch,
            GET_KOTA,
            response.data ? response.data.rajaongkir.results : [],
          );
        }
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, GET_KOTA, error);

        // Tampilkan pesan error
        alert(error);
      });
  };
};

// Function getKotaDetail
export const getKotaDetail = kota_id => {
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, GET_KOTA_DETAIL);

    // Axios
    axios({
      method: 'get',
      url: API_RAJAONGKIR + 'city?id=' + kota_id,
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      .then(response => {
        if (response.status !== 200) {
          // Error
          dispatchError(dispatch, GET_KOTA_DETAIL, response);
        } else {
          // Sukses
          dispatchSuccess(
            dispatch,
            GET_KOTA_DETAIL,
            response.data ? response.data.rajaongkir.results : [],
          );
        }
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, GET_KOTA_DETAIL, error);

        // Tampilkan pesan error
        alert(error);
      });
  };
};

// Function postOngkir
export const postOngkir = (data, ekspedisi) => {
  console.log('Data :', data);
  console.log('Ekspedisi :', ekspedisi);
  return dispatch => {
    // Loading
    dispatchLoading(dispatch, POST_ONGKIR);

    const formData = new URLSearchParams();
    formData.append('origin', ORIGIN_CITY);

    // destination data.profile.kota
    formData.append('destination', data.profile.kota);

    // berat => data.totalBerat
    formData.append(
      'weight',
      data.totalBerat < 1 ? 1000 : data.totalBerat * 1000,
    );

    // courier => eskpedisi.kurir
    formData.append('courier', ekspedisi.kurir);

    axios({
      method: 'POST',
      url: API_RAJAONGKIR + 'cost',
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR_COST,
      data: formData,
      // Kalo berhasil jalankan .then
    })
      .then(response => {
        if (response.status !== 200) {
          // Error
          dispatchError(dispatch, POST_ONGKIR, response);
        } else {
          // Sukses
          const ongkirs = response.data.rajaongkir.results[0].costs;

          const ongkirYangDipilih = ongkirs
            .filter(ongkir => ongkir.service === ekspedisi.service)
            .map(filterOngkir => {
              return filterOngkir;
            });

          dispatchSuccess(dispatch, POST_ONGKIR, ongkirYangDipilih[0]);
        }
      })
      .catch(error => {
        // Error
        dispatchError(dispatch, POST_ONGKIR, error);

        // Tampilkan pesan error
        alert(error);
      });
  };
};
