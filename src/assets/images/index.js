import BackgroundImage from './bg-image.png';
import Logo from './logo.svg';
import Slider1 from './slider-image-1.png';
import User from './user.png';
import DefaultFoto from './default.jpg';
import KeranjangKosong from './keranjang-kosong.svg';

export { BackgroundImage, Logo, Slider1, User, DefaultFoto, KeranjangKosong };
