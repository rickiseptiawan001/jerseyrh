// icon button tab navigation
import IcHome from './ic-home.svg';
import IcHomeAktif from './ic-home-aktif.svg';

import IcJersey from './ic-jersey.svg';
import IcJerseyAktif from './ic-jersey-aktif.svg';

import IcProfile from './ic-profile.svg';
import IcProfileAktif from './ic-profile-aktif.svg';

// icon menu profile
import IcEditProfile from './ic-edit-profile.svg';
import IcChangePassword from './ic-change-password.svg';
import IcHistoryPemesanan from './ic-history-pemesanan.svg';
import IcLogout from './ic-logout.svg';

// icon arrow
import IcArrowLeft from './ic-arrow-left.svg';
import IcArrowRight from './ic-arrow-right.svg';

// icon keranjang
import IcKeranjang from './ic-keranjang.svg';
import IcCari from './ic-cari.svg';

// icon hapus keranjang
import IcHapus from './ic-hapus.svg';

// icon google
import IcGoogle from './ic-google.svg';

export {
  IcHome,
  IcHomeAktif,
  IcJersey,
  IcJerseyAktif,
  IcProfile,
  IcProfileAktif,
  IcEditProfile,
  IcChangePassword,
  IcHistoryPemesanan,
  IcLogout,
  IcArrowLeft,
  IcArrowRight,
  IcKeranjang,
  IcCari,
  IcHapus,
  IcGoogle,
};
