import LigaPremierLeague from './premierleague.png';
import LigaLaliga from './laliga.png';
import LigaSeriea from './seriea.png';
import LigaBundesLiga from './bundesliga.png';

export { LigaPremierLeague, LigaLaliga, LigaSeriea, LigaBundesLiga };
