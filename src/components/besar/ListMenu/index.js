import React from 'react';
import { StyleSheet, View } from 'react-native';
import { CardMenu } from '../../kecil';

const ListMenu = ({ menus, navigation }) => {
  return (
    <View style={styles.container}>
      {menus.map(menu => {
        return <CardMenu key={menu.id} menu={menu} navigation={navigation} />;
      })}
    </View>
  );
};

export default ListMenu;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 30,
  },
});
