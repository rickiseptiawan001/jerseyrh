import React, { Component } from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import { colors, fonts, responsiveHeight, getData } from '../../../utils';
import { IcCari } from '../../../assets';
import { Tombol, Jarak } from '../../kecil';
import { connect } from 'react-redux';
import { getListKeranjang } from '../../../actions/KeranjangAction';
import { saveKeywordJersey } from '../../../actions/JerseyAction';

class HeaderComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
    };
  }

  componentDidMount() {
    getData('user').then(res => {
      if (res) {
        // Sudah login dan tampilkan keranjang berdasarkan id user yang sedang login
        this.props.dispatch(getListKeranjang(res.uid));
      } else {
        //
      }
    });
  }

  // Function selesaiCari
  selesaiCari = () => {
    const { page, navigation, dispatch } = this.props;
    const { search } = this.state;

    // Jalankan action save keyword
    dispatch(saveKeywordJersey(search));

    // Jika itu halaman home kita navigate ke listJersey
    if (page !== 'ListJersey') {
      navigation.navigate('ListJersey');
    }

    // Kembalikan state search itu ke string kosong
    this.setState({
      search: '',
    });
  };

  render() {
    const { navigation, getListKeranjangResult } = this.props;
    const { search } = this.state;

    // Variable totalKeranjang
    let totalKeranjang;

    // Jika getListKeranjangResult ada (artinya jika keranjang ada isinya tidak kosong)
    if (getListKeranjangResult) {
      totalKeranjang = Object.keys(getListKeranjangResult.pesanans).length;
    }

    // Tampilkan total keranjang di console
    console.log('Total Keranjang', totalKeranjang);

    return (
      <View style={styles.container}>
        <View style={styles.wrapperHeader}>
          {/* Input pencarian */}
          <View style={styles.searchSection}>
            <IcCari />
            <TextInput
              placeholder="Cari nama klub    "
              value={search}
              style={styles.input}
              onChangeText={search => this.setState({ search })}
              onSubmitEditing={() => this.selesaiCari()}
            />
          </View>

          {/* Jarak */}
          <Jarak width={10} />

          {/* Tombol keranjang */}
          <Tombol
            icon="keranjang"
            padding={14}
            onPress={() => navigation.navigate('Keranjang')}
            totalKeranjang={totalKeranjang}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getListKeranjangResult: state.KeranjangReducer.getListKeranjangResult,
});

export default connect(mapStateToProps, null)(HeaderComponent);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    height: responsiveHeight(125),
  },
  input: {
    fontSize: 14,
    fontFamily: fonts.light,
    paddingLeft: 10,
    marginTop: 5,
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.white,
    borderRadius: 50,
    paddingLeft: 20,
    alignItems: 'center',
  },
  wrapperHeader: {
    flexDirection: 'row',
    marginTop: 15,
    marginHorizontal: 30,
  },
});
