import React from 'react';
import { View, StyleSheet, ScrollView, ActivityIndicator, Text } from 'react-native';
import { CardHistoryPemesanan, Jarak } from '../../kecil';
import { connect } from 'react-redux';
import { colors } from '../../../utils';

const ListHistoryPemesanan = ({
  getListHistoryLoading,
  getListHistoryResult,
  getListHistoryError,
  navigation
}) => {
  return (
    <ScrollView showsVerticalScrollIndicator={true} >
      <View style={styles.container}>
        {getListHistoryResult ? (
          Object.keys(getListHistoryResult).map(key => {
            return (
              <CardHistoryPemesanan
                navigation={navigation}
                pesanan={getListHistoryResult[key]}
                key={key}
                id={key}
              />
            );
          })
        ) : getListHistoryLoading ? (
          <View style={styles.loading}>
            <ActivityIndicator color={colors.primary} />
          </View>
        ) : getListHistoryError ? (
          <Text>{getListHistoryError}</Text>
        ) : (
          <Text>Data Kosong</Text>
        )}

        {/*  jarak */}
        <Jarak height={30} />
      </View>
    </ScrollView >
  );
};

// mapStateToProps
const mapStateToProps = state => ({
  getListHistoryLoading: state.HistoryReducer.getListHistoryLoading,
  getListHistoryResult: state.HistoryReducer.getListHistoryResult,
  getListHistoryError: state.HistoryReducer.getListHistoryError,
});

// Connect redux
export default connect(mapStateToProps, null)(ListHistoryPemesanan);

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 30,
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
});
