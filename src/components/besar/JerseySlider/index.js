import React, { Component } from 'react';
import { StyleSheet, View, Modal } from 'react-native';
import { SliderBox } from 'react-native-image-slider-box';
import ImageViewer from 'react-native-image-zoom-viewer';
import { responsiveWidth, colors } from '../../../utils';

export default class JerseySlider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openImage: false,
      previewImage: false,
    };
  }

  // Function untuk menghandle ketika klik gambar jersey maka akan dizoom
  clickPreview = index => {
    this.setState({
      openImage: true,
      previewImage: [
        {
          url: this.props.images[index],
          props: {
            // Or you can set source directory.
            source: this.props.images[index],
          },
        },
      ],
    });
  };

  render() {
    const { images } = this.props;
    const { openImage, previewImage } = this.state;
    return (
      <View>
        <SliderBox
          images={images}
          sliderBoxHeight={200}
          ImageComponentStyle={styles.jersey}
          dotColor={colors.primary}
          imageLoadingColor={colors.primary}
          onCurrentImagePressed={index => this.clickPreview(index)}
        />

        <Modal visible={openImage} transparent={true}>
          <ImageViewer
            imageUrls={previewImage}
            backgroundColor={colors.primary}
            onClick={() => this.setState({ openImage: false })}
            enableSwipeDown
            onSwipeDown={() => this.setState({ openImage: false })}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  jersey: {
    marginTop: 70,
    width: responsiveWidth(200),
  },
});
