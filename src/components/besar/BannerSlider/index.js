import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Slider1 } from '../../../assets';
import { SliderBox } from 'react-native-image-slider-box';
import { colors, responsiveHeight, responsiveWidth } from '../../../utils';

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [Slider1, Slider1],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <SliderBox
          images={this.state.images}
          autoplay={true}
          circleLoop={true}
          sliderBoxHeight={responsiveHeight(133)}
          ImageComponentStyle={styles.slider}
          dotColor={colors.primary}
          imageLoadingColor={colors.primary}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  slider: {
    width: responsiveWidth(354),
    borderRadius: 10,
  },
  container: {
    marginTop: -15,
  },
});
