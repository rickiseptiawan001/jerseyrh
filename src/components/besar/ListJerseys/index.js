import React from 'react';
import { StyleSheet, View, Text, ActivityIndicator } from 'react-native';
import { CardJersey } from '../../kecil';
import { colors } from '../../../utils';
import { connect } from 'react-redux';

const ListJerseys = ({
  getListJerseyLoading,
  getListJerseyResult,
  getListJerseyError,
  navigation,
}) => {
  return (
    <View style={styles.container}>
      {getListJerseyResult ? (
        Object.keys(getListJerseyResult).map(key => {
          return (
            <CardJersey
              navigation={navigation}
              jersey={getListJerseyResult[key]}
              key={key}
              id={key}
            />
          );
        })
      ) : getListJerseyLoading ? (
        <View style={styles.loading}>
          <ActivityIndicator color={colors.primary} />
        </View>
      ) : getListJerseyError ? (
        <Text>{getListJerseyError}</Text>
      ) : (
        <Text>Data Kosong</Text>
      )}
    </View>
  );
};

// mapStateToProps
const mapStateToProps = state => ({
  getListJerseyLoading: state.JerseyReducer.getListJerseyLoading,
  getListJerseyResult: state.JerseyReducer.getListJerseyResult,
  getListJerseyError: state.JerseyReducer.getListJerseyError,
});

// Connect redux
export default connect(mapStateToProps, null)(ListJerseys);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
});
