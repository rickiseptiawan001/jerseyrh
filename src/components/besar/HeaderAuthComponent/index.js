import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { colors, responsiveHeight } from '../../../utils';

export default class HeaderAuthComponent extends Component {
  render() {
    return <View style={styles.container} />;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    height: responsiveHeight(433),
  },
});
