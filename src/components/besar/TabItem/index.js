import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import {
  IcHome,
  IcHomeAktif,
  IcJersey,
  IcJerseyAktif,
  IcProfile,
  IcProfileAktif,
} from '../../../assets';

import { colors, fonts } from '../../../utils';

const TabItem = ({ isFocused, onPress, onLongPress, label }) => {
  // Icons
  const Icon = () => {
    if (label === 'Home') {
      // Dicek apabila aktif nya di tab home, icon <IcHomeAktif /> yang ditampilkan
      return isFocused ? <IcHomeAktif /> : <IcHome />;
    } else if (label === 'Jersey') {
      // Dicek apabila aktif nya di tab jersey, icon <IcJerseyAktif /> yang ditampilkan
      return isFocused ? <IcJerseyAktif /> : <IcJersey />;
    } else if (label === 'Profile') {
      // Dicek apabila aktif nya di tab profile, icon <IcProfileAktif /> yang ditampilkan
      return isFocused ? <IcProfileAktif /> : <IcProfile />;
    } else {
      <IcHome />;
    }
  };
  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      {/* icon */}
      <Icon />
      <Text style={styles.text(isFocused)}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: isFocused => ({
    color: isFocused ? colors.primary : colors.nonAktif,
    fontSize: 14,
    marginTop: 4,
    fontFamily: fonts.light,
  }),
});
