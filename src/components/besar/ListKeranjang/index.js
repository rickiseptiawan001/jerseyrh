import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import { CardKeranjang, Jarak } from '../../kecil';
import { colors, fonts } from '../../../utils';
import { connect } from 'react-redux';
import { KeranjangKosong } from '../../../assets';

const ListKeranjang = ({
  getListKeranjangLoading,
  getListKeranjangResult,
  getListKeranjangError,
}) => {
  return (
    <ScrollView showsVerticalScrollIndicator={true}>
      <View>
        {getListKeranjangResult ? (
          Object.keys(getListKeranjangResult.pesanans).map(key => {
            return (
              <CardKeranjang
                keranjang={getListKeranjangResult.pesanans[key]}
                keranjangUtama={getListKeranjangResult}
                key={key}
                id={key}
              />
            );
          })
        ) : getListKeranjangLoading ? (
          <View style={styles.loading}>
            <ActivityIndicator color={colors.primary} />
          </View>
        ) : getListKeranjangError ? (
          <Text>{getListKeranjangError}</Text>
        ) : (
          <View style={styles.keranjangKosong}>
            <KeranjangKosong />
            <Text style={styles.textBold}>Opps !</Text>
            <Text style={styles.text}>Sepertinya keranjang kamu</Text>
            <Text style={styles.text2}>masih kosong</Text>
          </View>
        )}
      </View>
      <Jarak height={20} />
    </ScrollView>
  );
};

// mapStateToProps
const mapStateToProps = state => ({
  getListKeranjangLoading: state.KeranjangReducer.getListKeranjangLoading,
  getListKeranjangResult: state.KeranjangReducer.getListKeranjangResult,
  getListKeranjangError: state.KeranjangReducer.getListKeranjangError,
});

// Connect redux
export default connect(mapStateToProps, null)(ListKeranjang);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
  textBold: {
    marginTop: 30,
    fontFamily: fonts.semiBold,
    fontSize: 24,
    color: colors.darkBlue,
  },
  text: {
    marginTop: 14,
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.grey,
  },
  text2: {
    fontFamily: fonts.regular,
    fontSize: 16,
    color: colors.grey,
  },
  keranjangKosong: {
    alignItems: 'center',
    marginTop: 80,
  },
});
