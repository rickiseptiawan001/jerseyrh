import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { fonts, colors, responsiveWidth } from '../../../utils';
import Tombol from '../Tombol';
import Jarak from '../Jarak';

const CardJersey = ({ jersey, navigation }) => {
  return (
    <View style={styles.container}>
      {/* Card */}
      <View style={styles.card}>
        <Image source={{ uri: jersey.gambar[0] }} style={styles.gambar} />
        <Text numberOfLines={1} style={styles.title}>
          {jersey.nama}
        </Text>
      </View>

      {/* Tombol lihat detail */}
      <Tombol
        type="text"
        title="Lihat Detail"
        padding={6}
        fontSize={14}
        onPress={() => navigation.navigate('JerseyDetail', { jersey })}
      />
      <Jarak height={10} />
    </View>
  );
};

export default CardJersey;

const styles = StyleSheet.create({
  container: {
    marginBottom: 25,
  },
  card: {
    backgroundColor: colors.secondary,
    width: responsiveWidth(150),
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
  gambar: {
    width: 124,
    height: 124,
  },
  title: {
    fontFamily: fonts.regular,
    color: colors.darkBlue,
    fontSize: 14,
  },
});
