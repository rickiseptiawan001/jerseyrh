import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { colors, fonts } from '../../../utils';

const Inputan = ({
  textarea,
  width,
  height,
  fontSize,
  label,
  value,
  secureTextEntry,
  keyboardType,
  onChangeText,
  disabled,
  placeholder,
}) => {
  if (textarea) {
    return (
      <View style={styles.container}>
        <Text style={styles.label(fontSize)}>{label}</Text>
        <TextInput
          style={styles.inputTextArea(fontSize)}
          multiline={true}
          numberOfLines={4}
          value={value}
          onChangeText={onChangeText}
          editable={disabled ? false : true}
          placeholder={placeholder}
          onChangeText={onChangeText}
        />
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <Text style={styles.label(fontSize)}>{label}</Text>
      <TextInput
        style={styles.input(width, height, fontSize)}
        value={value}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
        onChangeText={onChangeText}
        editable={disabled ? false : true}
        placeholder={placeholder}
        onChangeText={onChangeText}
      />
    </View>
  );
};

export default Inputan;

const styles = StyleSheet.create({
  container: {
    // marginTop: 10,
  },
  label: fontSize => ({
    fontSize: fontSize ? fontSize : 18,
    fontFamily: fonts.regular,
    marginTop: 20,
    color: colors.darkBlue,
  }),
  input: (width, height, fontSize) => ({
    fontSize: fontSize ? fontSize : 15,
    fontFamily: fonts.regular,
    width: width,
    height: height,
    borderRadius: 50,
    backgroundColor: colors.backgroundInput,
    paddingHorizontal: 20,
    marginTop: 8,
  }),
  inputTextArea: fontSize => ({
    fontSize: fontSize ? fontSize : 15,
    fontFamily: fonts.regular,
    borderRadius: 10,
    backgroundColor: colors.backgroundInput,
    paddingVertical: 10,
    paddingHorizontal: 14,
    marginTop: 8,
    textAlignVertical: 'top',
  }),
});
