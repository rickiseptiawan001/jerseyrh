import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colors, fonts } from '../../../utils';

const CardAlamat = ({ alamat, provinsi, kota, navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.textBold}>Alamat saya :</Text>
      {/* Alamat */}
      <Text style={styles.alamat}>{alamat}</Text>

      {/* Kota */}
      <Text style={styles.alamat}>{kota}</Text>

      {/* Provinsi */}
      <Text style={styles.alamat}>{provinsi}</Text>

      {/* Tombol ubah alamat */}
      <TouchableOpacity
        style={styles.btnUbahAlamat}
        onPress={() => navigation.navigate('EditProfile')}>
        <Text style={styles.textUbahAlamat}>Ubah alamat</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CardAlamat;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    borderRadius: 10,
    padding: 14,
    marginTop: 10,
  },
  textBold: {
    fontSize: 14,
    marginBottom: 8,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  alamat: {
    fontSize: 14,
    color: colors.grey,
  },
  textUbahAlamat: {
    fontSize: 14,
    color: colors.primary,
    fontFamily: fonts.semiBold,
    textAlign: 'right',
  },
  btnUbahAlamat: {
    marginTop: 8,
  },
});
