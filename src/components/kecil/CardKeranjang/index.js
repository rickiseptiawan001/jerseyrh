import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import {
  colors,
  fonts,
  responsiveHeight,
  responsiveWidth,
  rupiah,
} from '../../../utils';
import { IcHapus } from '../../../assets';
import { deleteKeranjang } from '../../../actions/KeranjangAction';
import { connect } from 'react-redux';

const CardKeranjang = ({ keranjang, keranjangUtama, id, dispatch }) => {
  const hapusKeranjang = () => {
    dispatch(deleteKeranjang(id, keranjang, keranjangUtama));
  };
  return (
    <View style={styles.container}>
      {/* Image */}
      <Image
        source={{ uri: keranjang.product.gambar[0] }}
        style={styles.gambar}
      />

      {/* Desc */}
      <View style={styles.desc}>
        <Text numberOfLines={1} style={styles.nama}>{keranjang.product.nama}</Text>
        <Text style={styles.harga}>
          Harga : Rp. {rupiah(keranjang.product.harga)}
        </Text>

        <Text style={styles.text}>Pesan : {keranjang.jumlahPesan}</Text>
        <Text style={styles.text}>Ukuran : {keranjang.ukuran}</Text>
        <Text style={styles.text}>
          Total Harga : Rp. {rupiah(keranjang.totalHarga)}
        </Text>
      </View>

      {/* Tombol hapus */}
      <TouchableOpacity
        style={styles.tombolHapus}
        onPress={() => hapusKeranjang()}>
        <IcHapus />
      </TouchableOpacity>
    </View>
  );
};

export default connect()(CardKeranjang);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 30,
    backgroundColor: colors.secondary,
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderRadius: 10,
    alignItems: 'center',
    marginHorizontal: 30,
  },
  gambar: {
    width: 80,
    height: 80,
  },
  desc: {
    marginLeft: 16,
  },
  nama: {
    fontSize: 16,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  harga: {
    marginBottom: 20,
    color: colors.grey,
  },
  text: {
    color: colors.grey,
  },
  keterangan: {
    marginTop: 20,
    backgroundColor: colors.backgroundInput,
    paddingHorizontal: 14,
    paddingVertical: 8,
    borderRadius: 8,
  },
  tombolHapus: {
    flex: 1,
    alignItems: 'flex-end',
  },
});
