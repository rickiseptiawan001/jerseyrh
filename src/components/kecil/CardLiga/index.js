import React from 'react';
import { Image, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { colors } from '../../../utils';
import { getJerseyByLiga } from '../../../actions/JerseyAction';

const CardLiga = ({ liga, navigation, id, dispatch }) => {
  const toJerseyByLiga = (id, namaLiga) => {
    // ke Jersey Action
    dispatch(getJerseyByLiga(id, namaLiga));

    // navigate ke ListJersey
    navigation.navigate('ListJersey');
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => toJerseyByLiga(id, liga.namaLiga)}>
      <Image source={{ uri: liga.image }} style={styles.gambarLiga} />
    </TouchableOpacity>
  );
};

export default connect()(CardLiga);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    padding: 6,
    borderRadius: 10,
  },
  gambarLiga: {
    width: 55,
    height: 55,
  },
});
