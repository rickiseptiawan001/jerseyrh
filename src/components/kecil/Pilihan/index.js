import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { fonts, colors } from '../../../utils';
import { Picker } from '@react-native-picker/picker';

const Pilihan = ({
  datas,
  width,
  height,
  fontSize,
  label,
  fontFamily,
  selectedValue,
  onValueChange,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label(fontSize, fontFamily)}>{label}</Text>
      <View style={styles.wrapperPicker}>
        <Picker
          style={styles.picker(width, height, fontSize)}
          selectedValue={selectedValue}
          onValueChange={onValueChange}>
          <Picker.Item label={'--Pilih--'} value="" />
          {datas.map((item, index) => {
            if (label == 'Provinsi') {
              return (
                <Picker.Item
                  key={item.province_id}
                  label={item.province}
                  value={item.province_id}
                />
              );
            } else if (label == 'Kota / Kabupaten') {
              return (
                <Picker.Item
                  key={item.city_id}
                  label={item.type + ' ' + item.city_name}
                  value={item.city_id}
                />
              );
            } else if (label == 'Pilih Ekspedisi') {
              return (
                <Picker.Item key={item.id} label={item.label} value={item} />
              );
            } else {
              return <Picker.Item key={index} label={item} value={item} />;
            }
          })}
        </Picker>
      </View>
    </View>
  );
};

export default Pilihan;

const styles = StyleSheet.create({
  container: {
    // marginTop: 10,
  },
  label: (fontSize, fontFamily) => ({
    fontSize: fontSize ? fontSize : 18,
    fontFamily: fontFamily,
    marginTop: 20,
    color: colors.darkBlue,
  }),
  wrapperPicker: {
    borderRadius: 50,
    backgroundColor: colors.backgroundInput,
    marginTop: 8,
  },
  picker: (width, height, fontSize) => ({
    fontSize: fontSize ? fontSize : 15,
    fontFamily: fonts.regular,
    width: width,
    height: height,
    marginTop: -5,
    marginBottom: 5,
  }),
});
