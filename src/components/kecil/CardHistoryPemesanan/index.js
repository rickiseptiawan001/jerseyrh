import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { colors, fonts, rupiah } from '../../../utils';
import { connect } from 'react-redux';
import { updateStatus } from '../../../actions/HistoryAction';
import SweetAlert from 'react-native-sweet-alert';

class CardHistoryPemesanan extends Component {
  componentDidMount() {
    const { pesanan } = this.props;
    this.props.dispatch(updateStatus(pesanan.order_id));
  }

  masukMidtrans = () => {
    const { pesanan } = this.props;
    if (pesanan.status === 'lunas') {
      SweetAlert.showAlertWithOptions({
        title: 'Info!',
        subTitle: 'Pesanan sudah lunas',
        confirmButtonTitle: 'OK',
        style: 'success',
      });
    } else {
      this.props.navigation.navigate('Midtrans', { url: pesanan.url });
    }
  };

  render() {
    const { pesanan, updateStatusLoading } = this.props;
    const history = pesanan.pesanans;
    return (
      <TouchableOpacity style={styles.container} onPress={() => this.masukMidtrans()}>
        <Text style={styles.textBold}>Tanggal : {pesanan.tanggal}</Text>

        {/* perulangan */}
        {Object.keys(history).map((key, index) => {
          return (
            <View key={index} style={styles.history}>
              {/* Nomor */}
              <Text style={styles.nomor}>{index + 1}.</Text>

              {/* Image */}
              <Image source={{ uri: history[key].product.gambar[0] }} style={styles.foto} />

              {/* Desc */}
              <View style={styles.desc}>
                <Text style={styles.nama}>{history[key].product.nama}</Text>
                <Text style={styles.harga}>
                  Harga : Rp. {rupiah(history[key].product.harga)}
                </Text>

                <Text style={styles.text}>Pesan : {history[key].jumlahPesan}</Text>
                <Text style={styles.text}>Ukuran : {history[key].ukuran}</Text>
                <Text style={styles.text}>
                  Total Harga : Rp. {rupiah(history[key].totalHarga)}
                </Text>
              </View>
            </View>
          );
        })}

        {/* Footer */}
        <View style={styles.footer}>
          {/* Text bold */}
          <View>
            <Text style={styles.textBold}>Status : </Text>
            <Text style={styles.textBold}>Ongkir : </Text>
            <Text style={styles.textBold}>Estimasi : </Text>
            <Text style={styles.textBold}>Total Harga : </Text>
          </View>

          {/* Value */}
          <View style={styles.value}>
            <Text style={styles.textBold}>{updateStatusLoading ? 'Loading' : pesanan.status.toUpperCase()}</Text>
            <Text style={styles.textBold}>Rp {rupiah(pesanan.ongkir)}</Text>
            <Text style={styles.textBold}>{pesanan.estimasi} Hari</Text>
            <Text style={styles.textBold}>
              Rp {rupiah(pesanan.totalHarga + pesanan.ongkir)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

// mapStateToProps
const mapStateToProps = state => ({
  updateStatusLoading: state.HistoryReducer.updateStatusLoading
});

// Connect redux
export default connect(mapStateToProps, null)(CardHistoryPemesanan);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    padding: 15,
    borderRadius: 10,
    marginTop: 30,
  },
  history: {
    flexDirection: 'row',
    marginTop: 30
  },
  nomor: {
    fontSize: 16,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  desc: {
    marginLeft: 10,
  },
  foto: {
    width: 80,
    height: 80,
  },
  textBold: {
    color: colors.darkBlue,
    fontSize: 16,
    fontFamily: fonts.semiBold,
  },
  nama: {
    fontSize: 16,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  harga: {
    color: colors.darkBlue,
  },
  text: {
    color: colors.darkBlue,
  },
  keterangan: {
    marginTop: 20,
    backgroundColor: colors.backgroundInput,
    paddingHorizontal: 14,
    paddingVertical: 8,
    borderRadius: 8,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  value: {
    alignItems: 'flex-end',
  },
});
