import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import { colors, fonts } from '../../../utils';
import Jarak from '../Jarak';

const TombolLoading = ({ padding, fontSize }) => {
  return (
    <TouchableOpacity style={styles.container(padding)}>
      <ActivityIndicator size="small" color={colors.grey} />
      <Jarak width={5} />
      <Text style={styles.title(fontSize)}>Loading . . .</Text>
    </TouchableOpacity>
  );
};

export default TombolLoading;

const styles = StyleSheet.create({
  container: padding => ({
    backgroundColor: colors.backgroundInput,
    padding: padding,
    borderRadius: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  title: fontSize => ({
    color: colors.grey,
    fontSize: fontSize ? fontSize : 15,
    fontFamily: fonts.bold,
  }),
});
