import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { IcKeranjang, IcGoogle } from '../../../assets';
import { colors, fonts } from '../../../utils';
import Jarak from '../Jarak';

const TextIcon = ({ icon, padding, onPress, title, fontSize }) => {
  const Icon = () => {
    if (icon === 'keranjang') {
      return <IcKeranjang />;
    } else if (icon === 'google') {
      return <IcGoogle />;
    }
    return <IcKeranjang />;
  };

  return (
    <TouchableOpacity style={styles.container(padding)} onPress={onPress}>
      <Icon />
      <Jarak width={5} />
      <Text style={styles.title(fontSize)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default TextIcon;

const styles = StyleSheet.create({
  container: padding => ({
    backgroundColor: colors.backgroundInput,
    padding: padding,
    borderRadius: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  title: fontSize => ({
    color: colors.grey,
    fontSize: fontSize ? fontSize : 15,
    fontFamily: fonts.bold,
  }),
});
