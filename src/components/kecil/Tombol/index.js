import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { IcArrowLeft, IcKeranjang } from '../../../assets';
import { colors } from '../../../utils';
import TombolTextOnly from './TombolTextOnly';
import TombolLoading from './TombolLoading';
import TombolTextIcon from './TombolTextIcon';

const Tombol = props => {
  // Function Icon
  const Icon = () => {
    if (icon === 'keranjang') {
      return <IcKeranjang />;
    } else if (icon === 'arrow-left') {
      return <IcArrowLeft />;
    }

    return <IcKeranjang />;
  };

  // Props
  const { icon, padding, type, onPress, loading, totalKeranjang } = props;

  // Loading
  if (loading) {
    return <TombolLoading {...props} />;
  }

  // Type tombol text
  if (type === 'text') {
    return <TombolTextOnly {...props} />;
  }

  // Type tombol TextIcon
  if (type === 'textIcon') {
    return <TombolTextIcon {...props} />;
  }

  return (
    <TouchableOpacity style={styles.container(padding)} onPress={onPress}>
      <Icon />

      {totalKeranjang && (
        <View style={styles.notif}>
          <Text style={styles.textNotif}>{totalKeranjang}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

export default Tombol;

const styles = StyleSheet.create({
  container: padding => ({
    backgroundColor: colors.white,
    padding: padding,
    borderRadius: 50,
  }),
  notif: {
    position: 'absolute',
    top: 2,
    right: 2,
    backgroundColor: 'red',
    borderRadius: 6,
    paddingHorizontal: 3,
  },
  textNotif: {
    fontSize: 16,
    color: colors.white,
  },
});
