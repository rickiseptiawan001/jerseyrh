import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { colors, fonts } from '../../../utils';

const TextOnly = ({ padding, title, onPress, fontSize, warna, disabled }) => {
  return (
    <TouchableOpacity
      style={styles.container(padding, warna, disabled)}
      onPress={onPress}>
      <Text style={styles.text(fontSize, warna)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default TextOnly;

const styles = StyleSheet.create({
  container: (padding, warna, disabled) => ({
    backgroundColor: warna === 'secondary' ? colors.white : colors.primary,
    padding: padding,
    borderRadius: 50,
  }),
  text: (fontSize, warna) => ({
    color: warna === 'secondary' ? colors.darkBlue : colors.white,
    textAlign: 'center',
    fontSize: fontSize ? fontSize : 13,
    fontFamily: fonts.bold,
  }),
});
