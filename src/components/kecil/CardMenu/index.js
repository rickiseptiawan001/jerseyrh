import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { IcArrowRight } from '../../../assets';
import { colors, fonts, responsiveWidth, clearStorage } from '../../../utils';
import FIREBASE from '../../../config/FIREBASE';
import SweetAlert from 'react-native-sweet-alert';

const CardMenu = ({ menu, navigation }) => {
  // Function onSubmit
  const onSubmit = () => {
    // Jika halaman yang di akses adalah login jalankan code if
    if (menu.halaman === 'GetStarted') {
      FIREBASE.auth()
        .signOut()
        .then(function () {
          // Logout berhasil
          // Kemudian clear storage
          clearStorage();

          // Alihkan ke halaman GetStarted
          navigation.navigate('GetStarted');

          // Menampilkan pesan berhasil logout
          SweetAlert.showAlertWithOptions({
            title: 'SELAMAT!',
            subTitle: 'Logout berhasil!',
            confirmButtonTitle: 'OK',
            style: 'success',
          });
        })
        .catch(function (error) {
          // Tampilkan pesan jika ada error
          alert(error);
        });
    } else {
      navigation.navigate(menu.halaman);
    }
  };

  return (
    <TouchableOpacity style={styles.container} onPress={() => onSubmit()}>
      <View style={styles.menu}>
        <View style={styles.icon}>{menu.gambar}</View>
        <Text style={styles.textMenu}>{menu.nama}</Text>
      </View>
      <View style={styles.iconArrow}>
        <IcArrowRight />
      </View>
    </TouchableOpacity>
  );
};

export default CardMenu;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'space-between',
    backgroundColor: colors.backgroundInput,
    padding: 12,
    borderRadius: 50,
    alignItems: 'center',
  },
  textMenu: {
    fontFamily: fonts.regular,
    color: colors.darkBlue,
    fontSize: 18,
    marginLeft: 10,
  },
  menu: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconArrow: {
    width: responsiveWidth(25),
    marginRight: 10,
  },
  icon: {
    marginLeft: 10,
  },
});
