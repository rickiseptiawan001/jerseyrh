export const couriers = [
  // {
  //   id: 1,
  //   kurir: 'tiki',
  //   service: 'REG',
  //   label: 'TIKI (Regular Service)',
  // },
  // {
  //   id: 2,
  //   kurir: 'tiki',
  //   service: 'ECO',
  //   label: 'TIKI (Economy Service)',
  // },
  // {
  //   id: 3,
  //   kurir: 'tiki',
  //   service: 'ONS',
  //   label: 'TIKI (Over Night Service)',
  // },
  {
    id: 1,
    kurir: "jne",
    service: "OKE",
    label: "JNE (Ongkos Kirim Ekonomis)"
  },
  {
    id: 2,
    kurir: "jne",
    service: "REG",
    label: "JNE (Layanan Reguler)"
  },
  {
    id: 3,
    kurir: "jne",
    service: "YES",
    label: "JNE (Yakin Esok Sampai)"
  },
  // {
  //   id: 7,
  //   kurir: 'pos',
  //   service: 'Paket Kilat Khusus',
  //   label: 'POS (Paket Kilat Khusus)',
  // },
];
