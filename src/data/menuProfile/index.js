import React from 'react';
import {
  IcEditProfile,
  IcChangePassword,
  IcHistoryPemesanan,
  IcLogout,
} from '../../assets';

export const menuProfile = [
  {
    id: 1,
    nama: 'Edit Profile',
    gambar: <IcEditProfile />,
    halaman: 'EditProfile',
  },
  {
    id: 2,
    nama: 'Change Password',
    gambar: <IcChangePassword />,
    halaman: 'ChangePassword',
  },
  {
    id: 3,
    nama: 'History Pemesanan',
    gambar: <IcHistoryPemesanan />,
    halaman: 'HistoryPemesanan',
  },
  {
    id: 4,
    nama: 'Logout',
    gambar: <IcLogout />,
    halaman: 'GetStarted',
  },
];
