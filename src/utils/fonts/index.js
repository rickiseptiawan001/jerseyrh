export const fonts = {
  light: 'Poppins-Light',
  regular: 'Poppins-Regular',
  semiBold: 'Poppins-SemiBold',
  bold: 'Poppins-Bold',
};
