export const colors = {
  primary: '#0BCAD4',
  secondary: '#D5E7FF',
  nonAktif: '#495A75',
  darkBlue: '#112340',
  white: '#FFFFFF',
  red: '#D10214',
  grey: '#838383',
  backgroundInput: '#EDEEF0',
};
