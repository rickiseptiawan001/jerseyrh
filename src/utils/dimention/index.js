import { Dimensions } from 'react-native';
import { widthMobileUI, heightMobileUI } from '../constant';

// Variable windowWidth
export const windowWidth = Dimensions.get('window').width;

// windowHeight
export const windowHeight = Dimensions.get('window').height;

// Function responsiveWidth
export const responsiveWidth = width => {
  return (windowWidth * width) / widthMobileUI;
};

// Function responsiveHeight
export const responsiveHeight = height => {
  return (windowHeight * height) / heightMobileUI;
};
