// Lebar dan tinggi iPhone X
export const heightMobileUI = 896;
export const widthMobileUI = 414;

// Key api rajaongkir
export const API_TIMEOUT = 120000;
export const API_KEY = '77625f038da9e898c92652214ef9d5ed';
export const API_RAJAONGKIR = 'https://api.rajaongkir.com/starter/';
export const API_HEADER_RAJAONGKIR = {
  key: API_KEY,
};

// Untuk menghitung biaya ongkir secara otomatis
export const API_HEADER_RAJAONGKIR_COST = {
  key: API_KEY,
  'content-type': 'application/x-www-form-urlencoded',
};

// https://api.rajaongkir.com/starter/city <- GET di postman

// {
//  "city_id": "152",
//  "province_id": "6",
//  "province": "DKI Jakarta",
//  "type": "Kota",
//  "city_name": "Jakarta Pusat",
//  "postal_code": "10540"
// },

// Kota / kabupaten asal penjual
export const ORIGIN_CITY = '152';

// Header midtrans
export const HEADER_MIDTRANS = {
  Accept: 'application/json',
  'Content-Type': 'application/json',

  // sandbox
  // Authorization: 'Basic U0ItTWlkLXNlcnZlci1fN25ab3lwNTBLRWFLWm5oakhOQmt6REw=',

  // production
  Authorization: 'Basic TWlkLXNlcnZlci1Ia0Y0VmZHZGgxWTMyekluSndYTGVIVzU=',
};

// Url midtrans

// sandbox
// export const URL_MIDTRANS = 'https://app.sandbox.midtrans.com/snap/v1/';
// export const URL_MIDTRANS_STATUS = 'https://api.sandbox.midtrans.com/v2/';

// production
export const URL_MIDTRANS = 'https://app.midtrans.com/snap/v1/';
export const URL_MIDTRANS_STATUS = 'https://api.midtrans.com/v2/';
