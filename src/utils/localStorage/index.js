// https://react-native-async-storage.github.io/async-storage/docs/usage
import AsyncStorage from '@react-native-async-storage/async-storage';

// Function storeData
export const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    // saving error
  }
};

// Function getData
export const getData = async key => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      // Jika value tidak sama dengan null : arti simple nya 'jika ada datanya'
      return JSON.parse(value);
    }
  } catch (e) {
    // error reading value
  }
};

// Variable clearStorage
export const clearStorage = async () => {
  AsyncStorage.clear();
};
