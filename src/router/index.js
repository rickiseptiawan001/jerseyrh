import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  Home,
  Splash,
  GetStarted,
  ListJersey,
  Profile,
  Login,
  Register1,
  Register2,
  JerseyDetail,
  Keranjang,
  Checkout,
  EditProfile,
  ChangePassword,
  HistoryPemesanan,
  Midtrans,
} from '../pages';
import { BottomNavigator } from '../components';

const Stack = createNativeStackNavigator();

// bottom tab navigation
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
      {/* redirect to home */}
      <Tab.Screen name="Home" component={Home} options={{ headerShown: false }} />

      {/* redirect to jersey */}
      <Tab.Screen
        name="ListJersey"
        component={ListJersey}
        options={{ headerShown: false, title: 'Jersey' }}
      />

      {/* redirect to profile */}
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{ headerShown: false }}
      />
    </Tab.Navigator>
  );
};

// react navigation
const Router = () => {
  return (
    <Stack.Navigator initialRouteName={Splash}>
      {/* halaman splash screen */}
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />

      {/* halaman getstarted */}
      <Stack.Screen
        name="GetStarted"
        component={GetStarted}
        options={{ headerShown: false }}
      />

      {/* halaman login */}
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />

      {/* halaman register 1 */}
      <Stack.Screen
        name="Register1"
        component={Register1}
        options={{ headerShown: false }}
      />

      {/* halaman register 2 */}
      <Stack.Screen
        name="Register2"
        component={Register2}
        options={{ headerShown: false }}
      />

      {/* halaman jersey detail */}
      <Stack.Screen
        name="JerseyDetail"
        component={JerseyDetail}
        options={{ headerShown: false }}
      />

      {/* halaman keranjang */}
      <Stack.Screen name="Keranjang" component={Keranjang} />

      {/* halaman checkout */}
      <Stack.Screen name="Checkout" component={Checkout} />

      {/* halaman profile & edit profile */}
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{ title: 'Edit Profile' }}
      />

      {/* halaman change password */}
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{ title: 'Change Password' }}
      />

      {/* halaman history pemesanan */}
      <Stack.Screen
        name="HistoryPemesanan"
        component={HistoryPemesanan}
        options={{ title: 'History Pemesanan' }}
      />

      {/* halaman midtrans */}
      <Stack.Screen
        name="Midtrans"
        component={Midtrans}
        options={{ title: 'Lanjutkan Pembayaran' }}
      />

      {/* main app */}
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default Router;
