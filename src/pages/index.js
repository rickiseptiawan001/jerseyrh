import Home from './Home';
import Splash from './Splash';
import GetStarted from './GetStarted';
import Login from './Login';
import ListJersey from './ListJersey';
import Profile from './Profile';
import Register1 from './Register/Register1';
import Register2 from './Register/Register2';
import JerseyDetail from './JerseyDetail';
import Keranjang from './Keranjang';
import Checkout from './Checkout';
import EditProfile from './EditProfile';
import ChangePassword from './ChangePassword';
import HistoryPemesanan from './HistoryPemesanan';
import Midtrans from './Midtrans';

export {
  Home,
  Splash,
  GetStarted,
  Profile,
  ListJersey,
  Login,
  Register1,
  Register2,
  JerseyDetail,
  Keranjang,
  Checkout,
  EditProfile,
  ChangePassword,
  HistoryPemesanan,
  Midtrans,
};
