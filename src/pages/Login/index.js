import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { HeaderAuthComponent, Tombol, Jarak, Inputan } from '../../components';
import { colors, fonts, responsiveHeight } from '../../utils';
import SweetAlert from 'react-native-sweet-alert';
import { loginUser } from '../../actions/AuthAction';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };
  }

  // Akan di jalankan ketika component ini ada perubahan
  componentDidUpdate(prevProps) {
    const { loginResult } = this.props;

    if (loginResult && prevProps.loginResult !== loginResult) {
      // Pesan sukses login
      SweetAlert.showAlertWithOptions({
        title: 'SELAMAT!',
        subTitle: 'Login berhasil!',
        confirmButtonTitle: 'OK',
        style: 'success',
      });
      this.props.navigation.replace('MainApp');
    }
  }

  // Function login
  login = () => {
    const { email, password } = this.state;
    if (email && password) {
      // Action
      this.props.dispatch(loginUser(email, password));
    } else {
      SweetAlert.showAlertWithOptions({
        title: 'Maaf!',
        subTitle: 'Form tidak boleh kosong',
        confirmButtonTitle: 'OK',
        style: 'error',
      });
    }
  };

  render() {
    const { email, password } = this.state;
    const { loginLoading } = this.props;
    return (
      <View style={styles.page}>
        <ScrollView>
          <HeaderAuthComponent />
          <View style={styles.wrapperHeader}>
            <Text style={styles.headingTitle}>Login Page</Text>
            <View style={styles.card}>
              <View style={styles.form}>
                {/* Email */}
                <Inputan
                  label="Email Address"
                  placeholder="Masukan email"
                  value={email}
                  onChangeText={email => this.setState({ email })}
                />

                {/* Password */}
                <Inputan
                  label="Password"
                  secureTextEntry
                  placeholder="Masukan password"
                  value={password}
                  onChangeText={password => this.setState({ password })}
                />

                {/* Jarak */}
                <Jarak height={30} />

                {/* Tombol login */}
                <Tombol
                  type="text"
                  title="Login"
                  padding={8}
                  fontSize={18}
                  onPress={() => this.login()}
                  loading={loginLoading}
                />

                {/* Jarak */}
                {/* <Jarak height={20} /> */}

                {/* Tombol login with Google */}
                {/* <Tombol
                  type="textIcon"
                  icon="google"
                  title="Login with Google"
                  padding={8}
                  fontSize={18}
                /> */}
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

// Function mapStateToProps
const mapStateToProps = state => ({
  loginLoading: state.AuthReducer.loginLoading,
  loginResult: state.AuthReducer.loginResult,
  loginError: state.AuthReducer.loginError,
});

// Connect redux
export default connect(mapStateToProps, null)(Login);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  wrapperHeader: {
    marginHorizontal: 30,
    marginTop: -250,
  },
  headingTitle: {
    fontFamily: fonts.bold,
    fontSize: 34,
    color: colors.white,
    marginBottom: 20,
  },
  card: {
    backgroundColor: colors.white,
    height: responsiveHeight(400),
    borderRadius: 10,
    borderWidth: 0.4,
    borderColor: colors.darkBlue,
    marginBottom: 30,
  },
  form: {
    padding: 30,
    marginTop: -20,
  },
});
