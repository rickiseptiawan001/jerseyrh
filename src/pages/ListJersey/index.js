import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { HeaderComponent, ListLiga, ListJerseys } from '../../components';
import { colors, fonts } from '../../utils';
import { connect } from 'react-redux';
import { getListLiga } from '../../actions/LigaAction';
import { getListJersey } from '../../actions/JerseyAction';

class ListJersey extends Component {
  /*Code ini tujuannya biar ketika klik menu profile maka componentDidMount akan dijalankan
    karna jika tidak menggunakan code dibawah ini maka secara default componentDidMount tidak 
    akan dijalankan yang ada di tab item seperti(Home, Jersey, Profile)*/

  // https://reactnavigation.org/docs/navigation-events/
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      const { idLiga, keyword } = this.props;
      this.props.dispatch(getListLiga());
      this.props.dispatch(getListJersey(idLiga, keyword));
    });
  }

  // https://reactnavigation.org/docs/navigation-events/
  componentWillUnmount() {
    this._unsubscribe();
  }

  // Akan di jalankan ketika component ini ada perubahan
  componentDidUpdate(prevProps) {
    const { idLiga, keyword } = this.props;

    if (idLiga && prevProps.idLiga !== idLiga) {
      this.props.dispatch(getListJersey(idLiga));
    }

    if (keyword && prevProps.keyword !== keyword) {
      this.props.dispatch(getListJersey(idLiga, keyword));
    }
  }

  render() {
    const { navigation, namaLiga, keyword } = this.props;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.page}>
          {/* Component HeaderComponent */}
          <HeaderComponent navigation={navigation} />

          {/* Liga */}
          <View style={styles.pilihLiga}>
            <ListLiga navigation={navigation} />
          </View>

          {/* Jersey */}
          <View style={styles.pilihJersey}>
            <Text style={styles.label}>
              Pilih Jersey {namaLiga ? namaLiga : ''}
            </Text>
            <ListJerseys navigation={navigation} />
          </View>
        </View>
      </ScrollView>
    );
  }
}

// mapStateToProps
const mapStateToProps = state => ({
  idLiga: state.JerseyReducer.idLiga,
  namaLiga: state.JerseyReducer.namaLiga,
  keyword: state.JerseyReducer.keyword,
});

// Connect redux
export default connect(mapStateToProps, null)(ListJersey);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  pilihLiga: {
    marginHorizontal: 30,
    marginTop: -20,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.semiBold,
    marginTop: 30,
    marginBottom: 18,
  },
  pilihJersey: {
    marginHorizontal: 30,
  },
});
