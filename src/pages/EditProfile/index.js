import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image, Alert } from 'react-native';
import { colors, fonts } from '../../utils';
import { Inputan, Pilihan, Tombol, Jarak } from '../../components';
import { responsiveHeight, responsiveWidth, getData } from '../../utils';
import { getProvinsiList, getKotaList } from '../../actions/RajaOngkirAction';
import { updateProfile } from '../../actions/ProfileAction';
import { DefaultFoto } from '../../assets';
import { connect } from 'react-redux';
import { launchImageLibrary } from 'react-native-image-picker';
import SweetAlert from 'react-native-sweet-alert';

class EditProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      uid: '',
      nama: '',
      email: '',
      nohp: '',
      alamat: '',
      provinsi: false,
      kota: false,
      avatar: false,
      avatarForDB: '',
      avatarLama: '',
      updateAvatar: false,
    };
  }

  // Component ini pertama kali dijalankan ketika mengakses halaman ini
  componentDidMount() {
    this.getUserData();
    this.props.dispatch(getProvinsiList());
  }

  // Component ini akan dijalankan apabila ada suatu perubahan di halaman ini
  componentDidUpdate(prevProps) {
    const { updateProfileResult } = this.props;

    if (
      updateProfileResult &&
      prevProps.updateProfileResult !== updateProfileResult
    ) {
      // Pesan sukses update
      SweetAlert.showAlertWithOptions({
        title: 'SELAMAT!',
        subTitle: 'Edit profile berhasil!',
        confirmButtonTitle: 'OK',
        style: 'success',
      });
      this.props.navigation.replace('MainApp');
    }
  }

  // Function getUserData
  getUserData = () => {
    getData('user').then(res => {
      const data = res;
      this.setState({
        uid: data.uid,
        nama: data.nama,
        email: data.email,
        nohp: data.nohp,
        alamat: data.alamat,
        kota: data.kota,
        provinsi: data.provinsi,
        avatar: data.avatar,
        avatarLama: data.avatar,
      });

      this.props.dispatch(getKotaList(data.provinsi));
    });
  };

  // Function ubahProvinsi
  ubahProvinsi = provinsi_id => {
    this.setState({
      provinsi: provinsi_id,
    });

    this.props.dispatch(getKotaList(provinsi_id));
  };

  // Function getImage
  getImage = () => {
    launchImageLibrary(
      {
        quality: 1,
        maxWidth: 500,
        maxHeight: 500,
        includeBase64: true,
        selectionLimit: 1,
        cameraType: 'front',
      },
      response => {
        if (response.didCancel || response.errorCode || response.errorMessage) {
          Alert.alert('Error', 'Maaf sepertinya anda tidak memilih fotonya');
        } else {
          // Untuk storage
          const source = response.assets[0].uri;

          // Untuk database di realtime database firebase
          const fileString = `data:${response.assets[0].type};base64,${response.assets[0].base64}`;

          // Kita ubah value nya
          this.setState({
            avatar: source,
            avatarForDB: fileString,
            updateAvatar: true,
          });
        }
      },
    );
  };

  // Function onSubmit
  onSubmit = () => {
    const { nama, email, nohp, alamat, provinsi, kota } = this.state;

    if (nama && email && nohp && alamat && provinsi && kota) {
      // update data profile yang baru
      this.props.dispatch(updateProfile(this.state));
    } else {
      SweetAlert.showAlertWithOptions({
        title: 'Maaf!',
        subTitle: 'Form tidak boleh kosong',
        confirmButtonTitle: 'OK',
        style: 'error',
      });
    }
  };

  render() {
    const { nama, email, nohp, alamat, provinsi, kota, avatar } = this.state;
    const { getProvinsiResult, getKotaResult, updateProfileLoading } = this.props;
    return (
      <View style={styles.pages}>
        <ScrollView showsVerticalScrollIndicator={true}>
          <View style={styles.form}>
            {/* Nama lengkap */}
            <Inputan
              label="Nama Lengkap"
              value={nama}
              onChangeText={nama => this.setState({ nama })}
            />

            {/* Email address */}
            <Inputan
              disabled
              label="Email Address"
              value={email}
              onChangeText={email => this.setState({ email })}
            />

            {/* No hp */}
            <Inputan
              label="No HP"
              keyboardType="number-pad"
              value={nohp}
              onChangeText={nohp => this.setState({ nohp })}
            />

            {/* Alamat */}
            <Inputan
              textarea
              label="Alamat"
              value={alamat}
              onChangeText={alamat => this.setState({ alamat })}
            />

            {/* Provinsi */}
            <Pilihan
              label="Provinsi"
              fontSize={18}
              fontFamily={fonts.regular}
              // Jika ada datanya tampilkan getProvinsiResult
              // Jika tidak ada datanya tampilkan [] array kosong
              datas={getProvinsiResult ? getProvinsiResult : []}
              selectedValue={provinsi}
              onValueChange={provinsi_id => this.ubahProvinsi(provinsi_id)}
              height={responsiveHeight(55)}
            />

            {/* Kota / kabupaten  */}
            <Pilihan
              label="Kota / Kabupaten"
              fontSize={18}
              fontFamily={fonts.regular}
              datas={getKotaResult ? getKotaResult : []}
              selectedValue={kota}
              onValueChange={kota => this.setState({ kota: kota })}
              height={responsiveHeight(55)}
            />

            {/* Foto */}
            <View style={styles.inputFoto}>
              <View style={styles.wrapperUpload}>
                {/* Kalo ada foto nya tampilkan uri: avatar, kalo gak ada fotonya tampilkan DefaultImage  */}
                <Image
                  source={avatar ? { uri: avatar } : DefaultFoto}
                  style={styles.foto}
                />

                <Tombol
                  type="text"
                  title="Change Photo"
                  padding={8}
                  paddingHorizontal={20}
                  onPress={() => this.getImage()}
                />
              </View>
            </View>

            {/* Jarak */}
            <Jarak height={30} />

            {/* Tombol simpan perubahan */}
            <Tombol
              type="text"
              title="Simpan Perubahan"
              fontSize={18}
              padding={10}
              onPress={() => this.onSubmit()}
              loading={updateProfileLoading}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getProvinsiResult: state.RajaOngkirReducer.getProvinsiResult,
  getKotaResult: state.RajaOngkirReducer.getKotaResult,

  updateProfileLoading: state.ProfileReducer.updateProfileLoading,
  updateProfileResult: state.ProfileReducer.updateProfileResult,
  updateProfileError: state.ProfileReducer.updateProfileError,
});

export default connect(mapStateToProps, null)(EditProfile);

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
  },
  form: {
    padding: 30,
    marginTop: -20,
  },
  inputFoto: {
    marginTop: 30,
  },
  wrapperUpload: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  foto: {
    width: responsiveWidth(150),
    height: responsiveWidth(150),
    borderRadius: 10,
    alignSelf: 'center',
  },
});
