import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { ListKeranjang, Tombol } from '../../components';
import { colors, fonts, getData, rupiah } from '../../utils';
import { getListKeranjang } from '../../actions/KeranjangAction';
import { connect } from 'react-redux';
import SweetAlert from 'react-native-sweet-alert';

class Keranjang extends Component {
  componentDidMount() {
    getData('user').then(res => {
      if (res) {
        // Sudah login dan tampilkan keranjang berdasarkan id user yang sedang login
        this.props.dispatch(getListKeranjang(res.uid));
      } else {
        // Tampilkan pesan
        SweetAlert.showAlertWithOptions({
          title: 'Maaf!',
          subTitle: 'Silahkan login terlebih dahulu',
          confirmButtonTitle: 'OK',
          style: 'error',
        });

        /* Jika data user nya tidak ada maka kita tendang ke halaman login, 
                artinya belum login atau belum daftar */
        this.props.navigation.replace('GetStarted');
      }
    });
  }

  // Component ini akan dijalankan apabila ada suatu perubahan di halaman ini
  componentDidUpdate(prevProps) {
    const { deleteKeranjangResult } = this.props;

    if (
      deleteKeranjangResult &&
      prevProps.deleteKeranjangResult !== deleteKeranjangResult
    ) {
      // Pesan sukses update
      SweetAlert.showAlertWithOptions({
        title: 'SELAMAT!',
        subTitle: 'Hapus keranjang berhasil!',
        confirmButtonTitle: 'OK',
        style: 'success',
      });
      this.props.navigation.replace('Keranjang');
    }
  }

  render() {
    const { getListKeranjangResult } = this.props;
    return (
      <View style={styles.page}>
        {/* Component ListKeranjang */}
        {/* ..this.props <- kirim 
                getListKeranjangLoading, 
                getListKeranjangResult, 
                getListKeranjangError */}
        <ListKeranjang {...this.props} />

        <View style={styles.footer}>
          {/* Total harga */}
          <View style={styles.totalHarga}>
            <Text style={styles.textHarga}>Total Harga :</Text>
            <Text style={styles.textBold}>
              Rp{' '}
              {getListKeranjangResult
                ? rupiah(getListKeranjangResult.totalHarga)
                : '0'}
            </Text>
          </View>

          {/* Tombol */}
          {getListKeranjangResult ? (
            <Tombol
              type="text"
              title="Checkout"
              fontSize={18}
              padding={10}
              onPress={() =>
                this.props.navigation.navigate('Checkout', {
                  totalHarga: getListKeranjangResult.totalHarga,
                  totalBerat: getListKeranjangResult.totalBerat,
                })
              }
            />
          ) : (
            <View style={styles.btnDisable}>
              <Text style={styles.text}>Checkout</Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getListKeranjangLoading: state.KeranjangReducer.getListKeranjangLoading,
  getListKeranjangResult: state.KeranjangReducer.getListKeranjangResult,
  getListKeranjangError: state.KeranjangReducer.getListKeranjangError,

  deleteKeranjangLoading: state.KeranjangReducer.deleteKeranjangLoading,
  deleteKeranjangResult: state.KeranjangReducer.deleteKeranjangResult,
  deleteKeranjangError: state.KeranjangReducer.deleteKeranjangError,
});

export default connect(mapStateToProps, null)(Keranjang);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  footer: {
    paddingHorizontal: 30,
    paddingBottom: 30,
    borderTopWidth: 0.5,
  },
  totalHarga: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textHarga: {
    fontSize: 18,
    marginVertical: 30,
    fontFamily: fonts.regular,
    color: colors.darkBlue,
  },
  textBold: {
    fontSize: 18,
    marginVertical: 30,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  btnDisable: {
    padding: 10,
    borderRadius: 50,
    backgroundColor: colors.backgroundInput,
  },
  text: {
    fontFamily: fonts.regular,
    fontSize: 18,
    textAlign: 'center',
  },
});
