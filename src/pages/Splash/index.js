import React, { Component } from 'react';
import { Text, StyleSheet, View, ImageBackground } from 'react-native';
import { Logo, BackgroundImage } from '../../assets';
import { colors, fonts } from '../../utils';

export default class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('MainApp');
    }, 3000);
  }

  render() {
    return (
      <ImageBackground source={BackgroundImage} style={styles.page}>
        <View>
          {/* Component Logo */}
          <Logo />
          <Text style={styles.title}>Selamat datang di Toko Jersey RH</Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  page: {
    padding: 40,
    justifyContent: 'space-between',
    flex: 1,
  },

  title: {
    fontSize: 28,
    color: colors.white,
    marginTop: 300,
    fontFamily: fonts.bold,
  },
});
