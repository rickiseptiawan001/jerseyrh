import React, { Component } from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import { CardAlamat, Pilihan, Jarak, Tombol } from '../../components';
import { colors, fonts, rupiah, responsiveHeight, getData } from '../../utils';
import { connect } from 'react-redux';
import { getKotaDetail, postOngkir } from '../../actions/RajaOngkirAction';
import { snapTransactions } from '../../actions/PaymentAction'
import { couriers } from '../../data';
import SweetAlert from 'react-native-sweet-alert';

class Checkout extends Component {
  // constructor
  constructor(props) {
    super(props);

    this.state = {
      profile: false,
      ekspedisi: couriers,
      ekspedisiSelected: false,
      ongkir: 0,
      estimasi: '',
      // Mendapatkan data dari halaman sebelumnya yaitu halaman keranjang
      totalHarga: this.props.route.params.totalHarga,
      totalBerat: this.props.route.params.totalBerat,
      kota: '',
      provinsi: '',
      alamat: '',
      currentDate: new Date().getTime()
    };
  }

  // componentDidMount
  componentDidMount() {
    this.getUserData();
  }

  // Component ini akan dijalankan apabila ada suatu perubahan di halaman ini
  componentDidUpdate(prevProps) {
    const { getKotaDetailResult, ongkirResult, snapTransactionsResult } = this.props;

    if (
      getKotaDetailResult &&
      prevProps.getKotaDetailResult !== getKotaDetailResult
    ) {
      this.setState({
        provinsi: getKotaDetailResult.province,
        kota: getKotaDetailResult.type + ' ' + getKotaDetailResult.city_name,
      });
    }

    if (ongkirResult && prevProps.ongkirResult !== ongkirResult) {
      this.setState({
        ongkir: ongkirResult.cost[0].value,
        estimasi: ongkirResult.cost[0].etd,
      });
    }

    if (snapTransactionsResult && prevProps.snapTransactionsResult !== snapTransactionsResult) {
      // console.log("Hasil", snapTransactionsResult)
      const param = {
        url: snapTransactionsResult.redirect_url,
        ongkir: this.state.ongkir,
        estimasi: this.state.estimasi,
        order_id: "ORDER-" + this.state.currentDate + "-" + this.state.profile.uid,
      }

      this.props.navigation.navigate('Midtrans', param)
    }
  }

  // Arrow Function getUserData
  getUserData = () => {
    getData('user').then(res => {
      const data = res;

      // Jika datanya ada
      if (data) {
        this.setState({
          profile: data,
          alamat: data.alamat,
        });

        this.props.dispatch(getKotaDetail(data.kota));
      } else {
        // Tampilkan pesan
        SweetAlert.showAlertWithOptions({
          title: 'Maaf!',
          subTitle: 'Silahkan login terlebih dahulu',
          confirmButtonTitle: 'OK',
          style: 'error',
        });

        /* Jika data user nya tidak ada maka kita tendang ke halaman login, 
        artinya belum login atau belum daftar */
        this.props.navigation.replace('GetStarted');
      }
    });
  };

  // Arrow Function ubahEkspedisi
  ubahEkspedisi = ekspedisiSelected => {
    if (ekspedisiSelected) {
      this.setState({
        ekspedisiSelected: ekspedisiSelected,
      });

      this.props.dispatch(postOngkir(this.state, ekspedisiSelected));
    }
  };

  // Arrow Function bayarSekarang
  bayarSekarang = () => {
    // State 
    const { totalHarga, ongkir, profile, currentDate } = this.state
    // Object data
    const data = {
      // Midtrans
      transaction_details: {
        order_id: "ORDER-" + currentDate + "-" + profile.uid,
        gross_amount: parseInt(totalHarga + ongkir)
      },
      credit_card: {
        secure: true
      },
      customer_details: {
        first_name: profile.nama,
        email: profile.email,
        phone: profile.nohp,
      }
    }

    // console.log
    // console.log("isi object data", data)

    // Validasi form
    if (!ongkir == 0) {
      // snapTransaction
      this.props.dispatch(snapTransactions(data))
    } else {
      // Sweetalert
      SweetAlert.showAlertWithOptions({
        title: 'Maaf!',
        subTitle: 'Pilih ekspedisi terlebih dahulu',
        confirmButtonTitle: 'OK',
        style: 'error',
      });
    }
  }

  render() {
    const {
      totalHarga,
      totalBerat,
      alamat,
      kota,
      provinsi,
      ekspedisi,
      ekspedisiSelected,
      ongkir,
      estimasi,
    } = this.state;

    const { navigation, snapTransactionsLoading } = this.props;

    return (
      <View style={styles.pages}>
        {console.log('ongkir', ongkir)}
        {console.log('estimasi', estimasi)}
        <ScrollView showsVerticalScrollIndicator={true}>
          <View style={styles.isi}>
            <Text style={styles.textBold}>Apakah sudah benar alamat ini ?</Text>

            {/* Component CardAlamat */}
            <CardAlamat
              alamat={alamat}
              provinsi={provinsi}
              kota={kota}
              navigation={navigation}
            />

            {/* Jarak */}
            <Jarak height={10} />

            {/* Pilihan ekspedisi */}
            <Pilihan
              label="Pilih Ekspedisi"
              fontSize={18}
              fontFamily={fonts.semiBold}
              datas={ekspedisi}
              height={responsiveHeight(50)}
              selectedValue={ekspedisiSelected}
              onValueChange={ekspedisiSelected =>
                this.ubahEkspedisi(ekspedisiSelected)
              }
            />

            {/* Jarak */}
            <Jarak height={30} />

            {/* Total harga */}
            <View>
              <Text style={styles.textBold}>Total Harga</Text>
              <View style={styles.totalHarga}>
                <Text style={styles.textLeft}>Totalnya seharga</Text>
                <Text style={styles.textRight}>Rp {rupiah(totalHarga)}</Text>
              </View>
            </View>

            {/* Jarak */}
            <Jarak height={30} />

            {/* Biaya ongkir */}
            <View>
              <Text style={styles.textBold}>Biaya Ongkir</Text>

              {/* Berat */}
              <View style={styles.biayaOngkir}>
                <Text style={styles.textLeft}>Untuk Berat {totalBerat} kg</Text>
                <Text style={styles.textRight}>Rp {rupiah(ongkir)}</Text>
              </View>

              {/* Estimasi waktu */}
              <View style={styles.biayaOngkir}>
                <Text style={styles.textLeft}>Estimasi Waktu</Text>
                <Text style={styles.textRight}>{estimasi} Hari</Text>
              </View>
            </View>
          </View>

          {/* Jarak */}
          <Jarak height={30} />
        </ScrollView>

        <View style={styles.footer}>
          {/* Total harga */}
          <View style={styles.totalHarga}>
            <Text style={styles.textHarga}>Total + Ongkir :</Text>
            <Text style={styles.textBold2}>
              Rp {rupiah(totalHarga + ongkir)}
            </Text>
          </View>

          {/* Tombol bayar sekarang */}
          <Tombol
            type={'text'}
            title="Bayar Sekarang"
            fontSize={18}
            padding={10}
            onPress={() => this.bayarSekarang()}
            loading={snapTransactionsLoading}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  getKotaDetailResult: state.RajaOngkirReducer.getKotaDetailResult,
  getKotaDetailLoading: state.RajaOngkirReducer.getKotaDetailLoading,
  getKotaDetailError: state.RajaOngkirReducer.getKotaDetailError,

  ongkirResult: state.RajaOngkirReducer.ongkirResult,

  snapTransactionsResult: state.PaymentReducer.snapTransactionsResult,
  snapTransactionsLoading: state.PaymentReducer.snapTransactionsLoading,
  snapTransactionsError: state.PaymentReducer.snapTransactionsError,
});

export default connect(mapStateToProps, null)(Checkout);

const styles = StyleSheet.create({
  pages: {
    backgroundColor: colors.white,
    flex: 1,
    // paddingTop: 30,
  },
  isi: {
    marginHorizontal: 30,
    paddingTop: 30,
  },
  textBold: {
    fontSize: 18,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  textBold2: {
    fontSize: 18,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
    marginVertical: 30,
  },
  totalHarga: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
  },
  biayaOngkir: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
  },
  textLeft: {
    fontSize: 16,
    fontFamily: fonts.regular,
    color: colors.grey,
  },
  textRight: {
    fontSize: 16,
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  textHarga: {
    fontSize: 18,
    marginVertical: 30,
    fontFamily: fonts.regular,
    color: colors.darkBlue,
  },
  footer: {
    paddingHorizontal: 30,
    paddingBottom: 30,
    borderTopWidth: 0.5,
  },
});
