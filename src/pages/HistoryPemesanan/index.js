import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { ListHistoryPemesanan } from '../../components';
import { getListHistory } from '../../actions/HistoryAction';
import { colors, getData } from '../../utils';

class HistoryPemesanan extends Component {
  // componentDidMount
  componentDidMount() {
    getData('user').then((res) => {
      const data = res;

      if (!data) {
        this.props.navigation.replace('Login');
      } else {
        this.props.dispatch(getListHistory(data.uid));
      }
    });
  }

  render() {
    // Props
    const { navigation } = this.props;
    return (
      <View style={styles.pages}>
        <ListHistoryPemesanan navigation={navigation} />
      </View>
    );
  }
}

// Connect redux
export default connect()(HistoryPemesanan)

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
