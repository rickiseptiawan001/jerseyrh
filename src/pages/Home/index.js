import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import {
  BannerSlider,
  HeaderComponent,
  ListLiga,
  ListJerseys,
} from '../../components';
import { colors, fonts } from '../../utils';
import { connect } from 'react-redux';
import { getListLiga } from '../../actions/LigaAction';
import { limitJersey } from '../../actions/JerseyAction';

class Home extends Component {
  /*Code ini tujuannya biar ketika klik menu profile maka componentDidMount akan dijalankan
    karna jika tidak menggunakan code dibawah ini maka secara default componentDidMount tidak 
    akan dijalankan yang ada di tab item seperti(Home, Jersey, Profile)*/

  // https://reactnavigation.org/docs/navigation-events/
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.props.dispatch(getListLiga());
      this.props.dispatch(limitJersey());
    });
  }

  // https://reactnavigation.org/docs/navigation-events/
  componentWillUnmount() {
    this._unsubscribe();
  }

  render() {
    const { navigation } = this.props;
    return (
      <ScrollView showsVerticalScrollIndicator={true}>
        <View style={styles.page}>
          {/* Component HeaderComponent */}
          <HeaderComponent navigation={navigation} />

          {/* Component BannerSlider */}
          <BannerSlider />

          {/* Liga */}
          <View style={styles.pilihLiga}>
            <Text style={styles.label}>Pilih Liga</Text>
            <ListLiga navigation={navigation} />
          </View>

          {/* Jersey */}
          <View style={styles.pilihJersey}>
            <Text style={styles.label}>Pilih Jersey</Text>
            <ListJerseys navigation={navigation} />
          </View>
        </View>
      </ScrollView>
    );
  }
}

// Connect redux
export default connect()(Home);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  pilihLiga: {
    marginHorizontal: 30,
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.semiBold,
    marginTop: 30,
    marginBottom: 18,
  },
  pilihJersey: {
    marginHorizontal: 30,
  },
});
