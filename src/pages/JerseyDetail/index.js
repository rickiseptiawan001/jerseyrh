import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import {
  colors,
  fonts,
  responsiveHeight,
  heightMobileUI,
  rupiah,
  responsiveWidth,
  getData,
} from '../../utils';
import {
  Tombol,
  CardLiga,
  JerseySlider,
  Inputan,
  Pilihan,
  Jarak,
} from '../../components';
import { RFValue } from 'react-native-responsive-fontsize';
import { getDetailLiga } from '../../actions/LigaAction';
import { masukKeranjang } from '../../actions/KeranjangAction';
import { connect } from 'react-redux';
import SweetAlert from 'react-native-sweet-alert';

class JerseyDetail extends Component {
  // Constructor
  constructor(props) {
    super(props);

    this.state = {
      jersey: this.props.route.params.jersey,
      images: this.props.route.params.jersey.gambar,

      // Form
      jumlah: '',
      ukuran: '',
      uid: '',
    };
  }

  componentDidMount() {
    const { jersey } = this.state;
    this.props.dispatch(getDetailLiga(jersey.liga));
  }

  // Component ini akan dijalankan apabila ada suatu perubahan di halaman ini
  componentDidUpdate(prevProps) {
    const { saveKeranjangResult } = this.props;

    if (
      saveKeranjangResult &&
      prevProps.saveKeranjangResult !== saveKeranjangResult
    ) {
      // Pesan sukses masuk keranjang
      SweetAlert.showAlertWithOptions({
        title: 'SELAMAT!',
        subTitle: 'Masuk keranjang berhasil!',
        confirmButtonTitle: 'OK',
        style: 'success',
      });
      this.props.navigation.navigate('Keranjang');
    }
  }

  // Function masukKeranjang
  masukKeranjang = () => {
    const { jumlah, ukuran } = this.state;

    getData('user').then(res => {
      // Jika data user ada (sudah login) jalankan code if
      if (res) {
        console.log(res);
        // Simpan uid local storage ke state
        this.setState({
          uid: res.uid,
        });

        // Validasi form
        if (jumlah && ukuran) {
          // Hubungkan ke action (keranjangAction/masukKeranjang)
          this.props.dispatch(masukKeranjang(this.state));
        } else {
          SweetAlert.showAlertWithOptions({
            title: 'Maaf!',
            subTitle: 'Form tidak boleh kosong',
            confirmButtonTitle: 'OK',
            style: 'error',
          });
        }
      } else {
        SweetAlert.showAlertWithOptions({
          title: 'Maaf!',
          subTitle: 'Silahkan login terlebih dahulu',
          confirmButtonTitle: 'OK',
          style: 'error',
        });
      }
    });
  };

  render() {
    // Props dan State
    const { navigation, getDetailLigaResult, saveKeranjangLoading } = this.props;
    const { jersey, images, jumlah, ukuran } = this.state;
    return (
      // Page
      <View style={styles.page}>
        {/* Tombol kembali ke halaman sebelumnya */}
        <View style={styles.buttonBack}>
          <Tombol
            padding={14}
            icon="arrow-left"
            onPress={() => navigation.goBack()}
          />
        </View>

        {/* Gambar jersey */}
        <JerseySlider images={images} />

        {/* Container */}
        <View style={styles.container}>
          {/* Card liga */}
          <View style={styles.liga}>
            <CardLiga
              liga={getDetailLigaResult}
              navigation={navigation}
              id={jersey.liga}
            />
          </View>

          {/* Description */}
          <ScrollView showsVerticalScrollIndicator={true}>
            <View style={styles.desc}>
              {/* Nama dan harga */}
              <Text style={styles.nama}>{jersey.nama}</Text>
              <Text style={styles.harga}>
                Harga : Rp {rupiah(jersey.harga)}
              </Text>

              {/* Test */}
              {/* <Text>{jumlah} - {ukuran}</Text> */}

              {/* Garis */}
              <View style={styles.garis} />

              {/* Jenis dan berat */}
              <View style={styles.jenisDanBerat}>
                <Text style={styles.jenis}>Jenis : {jersey.jenis}</Text>
                <Text style={styles.berat}>Berat : {jersey.berat} kg</Text>
              </View>

              {/* Wrapper input */}
              <View style={styles.wrapperInput}>
                {/* Inputan jumlah */}
                <Inputan
                  label="Jumlah"
                  fontSize={14}
                  placeholder="Masukan jumlah"
                  width={responsiveWidth(166)}
                  height={responsiveHeight(50)}
                  value={jumlah}
                  onChangeText={jumlah => this.setState({ jumlah })}
                  keyboardType="number-pad"
                />

                {/* Pilih ukuran */}
                <Pilihan
                  label="Pilih ukuran"
                  fontSize={14}
                  fontFamily={fonts.regular}
                  datas={jersey.ukuran}
                  width={responsiveWidth(166)}
                  height={responsiveHeight(50)}
                  onValueChange={ukuran => this.setState({ ukuran })}
                  selectedValue={ukuran}
                />
              </View>

              {/* Deskripsi */}
              <View style={styles.deskripsi}>
                <Text style={styles.label}>Deskripsi : </Text>
                <Text style={styles.value}>{jersey.deskripsi}</Text>
              </View>

              {/* Tombol masuk keranjang */}
              {jersey.ready == "true" || jersey.ready != "false" ? (
                <Tombol
                  type="text"
                  title="Masuk keranjang"
                  padding={10}
                  fontSize={18}
                  onPress={() => this.masukKeranjang()}
                  loading={saveKeranjangLoading}
                />
              ) : (
                <View style={styles.btnStokKosong}>
                  <Text style={styles.textStokKosong}>Stok Kosong</Text>
                </View>
              )}
            </View>

            {/*  Jarak */}
            <Jarak height={30} />
          </ScrollView>
        </View>
      </View>
    );
  }
}

// mapStateToProps
const mapStateToProps = state => ({
  getDetailLigaResult: state.LigaReducer.getDetailLigaResult,

  saveKeranjangLoading: state.KeranjangReducer.saveKeranjangLoading,
  saveKeranjangResult: state.KeranjangReducer.saveKeranjangResult,
  saveKeranjangError: state.KeranjangReducer.saveKeranjangError,
});

// Connect redux
export default connect(mapStateToProps, null)(JerseyDetail);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  container: {
    backgroundColor: colors.white,
    position: 'absolute',
    bottom: 0,
    height: responsiveHeight(520),
    width: '100%',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
  },
  liga: {
    alignItems: 'flex-end',
    marginRight: 30,
    marginTop: -35,
  },
  buttonBack: {
    position: 'absolute',
    marginTop: 30,
    marginLeft: 10,
    zIndex: 1,
  },
  desc: {
    marginHorizontal: 30,
    marginTop: -4,
  },
  garis: {
    borderWidth: 0.2,
    marginTop: 23,
    marginBottom: 10,
  },
  jenisDanBerat: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  jenis: {
    color: colors.darkBlue,
    fontFamily: fonts.regular,
  },
  deskripsi: {
    marginTop: 20,
  },
  label: {
    fontFamily: fonts.regular,
    color: colors.darkBlue,
  },
  value: {
    fontFamily: fonts.regular,
    marginTop: 10,
    marginBottom: 20,
    color: colors.darkBlue,
  },
  berat: {
    color: colors.darkBlue,
    fontFamily: fonts.regular,
  },
  nama: {
    fontSize: RFValue(24, heightMobileUI),
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
  },
  harga: {
    fontSize: RFValue(18, heightMobileUI),
    fontFamily: fonts.regular,
    color: colors.darkBlue,
  },
  wrapperInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnStokKosong: {
    padding: 10,
    borderRadius: 50,
    backgroundColor: colors.backgroundInput
  },
  textStokKosong: {
    alignSelf: 'center',
    fontSize: 18,
    fontFamily: fonts.semiBold
  }
});
