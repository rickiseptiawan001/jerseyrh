import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import {
  HeaderAuthComponent,
  Tombol,
  Jarak,
  Inputan,
  Pilihan,
} from '../../components';
import { colors, fonts, responsiveHeight } from '../../utils';
import { getProvinsiList, getKotaList } from '../../actions/RajaOngkirAction';
import SweetAlert from 'react-native-sweet-alert';
import { registerUser } from '../../actions/AuthAction';

class Register2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      alamat: '',
      kota: false,
      provinsi: false,
    };
  }

  // Pertama kali dijalankan ketika component ini di panggil
  componentDidMount() {
    this.props.dispatch(getProvinsiList());
  }

  // Akan di jalankan ketika component ini ada perubahan
  componentDidUpdate(prevProps) {
    const { registerResult } = this.props;

    if (registerResult && prevProps.registerResult !== registerResult) {
      // Pesan sukses register
      SweetAlert.showAlertWithOptions({
        title: 'SELAMAT!',
        subTitle: 'Register berhasil!',
        confirmButtonTitle: 'OK',
        style: 'success',
      });

      this.props.navigation.replace('MainApp');
    }
  }

  // Function ubahProvinsi
  ubahProvinsi = provinsi_id => {
    this.setState({
      provinsi: provinsi_id,
    });

    this.props.dispatch(getKotaList(provinsi_id));
  };

  // Function onContinue
  onContinue = () => {
    const { kota, provinsi, alamat } = this.state;

    // Jika form tidak kosong maka jalankan code 'if'
    if (kota && provinsi && alamat) {
      const data = {
        // Data diambil dari halaman register 1 yang dikirim sebelumnya
        nama: this.props.route.params.nama,
        email: this.props.route.params.email,
        nohp: this.props.route.params.nohp,

        // Data diambil dari halaman ini yaitu register 2 yang diketikan user
        alamat: alamat,
        provinsi: provinsi,
        kota: kota,

        // Jika user berhasil daftar, secara default status nya 'user'
        status: 'user',
      };

      // Ke AuthAction
      this.props.dispatch(registerUser(data, this.props.route.params.password));

      // Jika form kosong atau ada yang belum ke isi maka jalankan code 'else'
    } else {
      SweetAlert.showAlertWithOptions({
        title: 'Maaf!',
        subTitle: 'Form tidak boleh kosong',
        confirmButtonTitle: 'OK',
        style: 'error',
      });
    }
  };

  render() {
    const { kota, provinsi, alamat } = this.state;
    const { getProvinsiResult, getKotaResult, registerLoading } = this.props;
    // console.log("provinsi result", this.props.getProvinsiResult)
    // console.log("Parameter", this.props.route.params)
    return (
      <View style={styles.page}>
        <ScrollView showsVerticalScrollIndicator={true}>
          <HeaderAuthComponent />
          <View style={styles.wrapperHeader}>
            <Text style={styles.headingTitle}>Register Step 2</Text>
            <View style={styles.card}>
              <View style={styles.form}>
                {/* Alamat */}
                <Inputan
                  textarea
                  label="Alamat"
                  placeholder="Masukan alamat lengkap"
                  onChangeText={alamat => this.setState({ alamat })}
                  value={alamat}
                />

                {/* Provinsi */}
                <Pilihan
                  label="Provinsi"
                  fontSize={18}
                  fontFamily={fonts.regular}
                  // Jika ada datanya tampilkan getProvinsiResult
                  // Jika tidak ada datanya tampilkan [] array kosong
                  datas={getProvinsiResult ? getProvinsiResult : []}
                  selectedValue={provinsi}
                  onValueChange={provinsi_id => this.ubahProvinsi(provinsi_id)}
                  height={responsiveHeight(55)}
                />

                {/* Kota / kabupaten  */}
                <Pilihan
                  label="Kota / Kabupaten"
                  fontSize={18}
                  fontFamily={fonts.regular}
                  datas={getKotaResult ? getKotaResult : []}
                  selectedValue={kota}
                  onValueChange={kota => this.setState({ kota: kota })}
                  height={responsiveHeight(55)}
                />

                {/* Jarak */}
                <Jarak height={30} />

                {/* Tombol daftar */}
                <Tombol
                  type="text"
                  loading
                  title="Daftar Sekarang"
                  padding={8}
                  fontSize={18}
                  onPress={() => this.onContinue()}
                  loading={registerLoading}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

// mapStateToProps
const mapStateToProps = state => ({
  getProvinsiResult: state.RajaOngkirReducer.getProvinsiResult,
  getKotaResult: state.RajaOngkirReducer.getKotaResult,

  registerLoading: state.AuthReducer.registerLoading,
  registerResult: state.AuthReducer.registerResult,
  registerError: state.AuthReducer.registerError,
});

// Connect redux
export default connect(mapStateToProps, null)(Register2);

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  wrapperHeader: {
    marginHorizontal: 30,
    marginTop: -320,
  },
  headingTitle: {
    fontFamily: fonts.bold,
    fontSize: 34,
    color: colors.white,
    marginBottom: 20,
  },
  card: {
    backgroundColor: colors.white,
    height: responsiveHeight(540),
    borderRadius: 10,
    borderWidth: 0.4,
    borderColor: colors.darkBlue,
    marginBottom: 30,
  },
  form: {
    padding: 30,
    marginTop: -20,
  },
});
