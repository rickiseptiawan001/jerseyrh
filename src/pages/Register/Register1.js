import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { HeaderAuthComponent, Tombol, Jarak, Inputan } from '../../components';
import { colors, fonts, responsiveHeight } from '../../utils';
import SweetAlert from 'react-native-sweet-alert';

export default class Register1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nama: '',
      email: '',
      nohp: '',
      password: '',
    };
  }

  // Function onContinue
  onContinue = () => {
    const { nama, email, nohp, password } = this.state;
    if (nama && email && nohp && password) {
      // this.state <- mengirim data yang diketikan oleh user ke halaman register 2
      this.props.navigation.navigate('Register2', this.state);
    } else {
      SweetAlert.showAlertWithOptions({
        title: 'Maaf!',
        subTitle: 'Form tidak boleh kosong',
        confirmButtonTitle: 'OK',
        style: 'error',
      });
    }
  };

  render() {
    const { nama, email, nohp, password } = this.state;
    return (
      <View style={styles.page}>
        <ScrollView>
          <HeaderAuthComponent />
          <View style={styles.wrapperHeader}>
            <Text style={styles.headingTitle}>Register Step 1</Text>
            <View style={styles.card}>
              <View style={styles.form}>
                {/* Nama */}
                <Inputan
                  label="Nama Lengkap"
                  placeholder="Masukan nama lengkap"
                  value={nama}
                  onChangeText={nama => this.setState({ nama })}
                />

                {/* Email */}
                <Inputan
                  label="Email Address"
                  placeholder="Masukan email"
                  value={email}
                  onChangeText={email => this.setState({ email })}
                />

                {/* No hp */}
                <Inputan
                  label="No. HP"
                  keyboardType="number-pad"
                  placeholder="Masukan no handphone"
                  value={nohp}
                  onChangeText={nohp => this.setState({ nohp })}
                />

                {/* Password */}
                <Inputan
                  label="Password"
                  secureTextEntry
                  placeholder="Masukan password"
                  value={password}
                  onChangeText={password => this.setState({ password })}
                />

                {/* Jarak */}
                <Jarak height={30} />

                {/* Tombol Lanjut */}
                <Tombol
                  type="text"
                  title="Lanjut step 2"
                  padding={8}
                  fontSize={18}
                  onPress={() => this.onContinue()}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  wrapperHeader: {
    marginHorizontal: 30,
    marginTop: -320,
  },
  headingTitle: {
    fontFamily: fonts.bold,
    fontSize: 34,
    color: colors.white,
    marginBottom: 20,
  },
  card: {
    backgroundColor: colors.white,
    height: responsiveHeight(630),
    borderRadius: 10,
    borderWidth: 0.4,
    borderColor: colors.darkBlue,
    marginBottom: 30,
  },
  form: {
    padding: 30,
    marginTop: -20,
  },
});
