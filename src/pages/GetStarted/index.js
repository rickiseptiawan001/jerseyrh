import React from 'react';
import { Text, StyleSheet, View, ImageBackground } from 'react-native';
import { Logo, BackgroundImage } from '../../assets';
import { Tombol, Jarak } from '../../components';

const GetStarted = ({ navigation }) => {
  return (
    <ImageBackground source={BackgroundImage} style={styles.page}>
      <View>
        <Logo />
        <Text style={styles.title}>
          Dapatkan jersey dengan harga terjangkau
        </Text>
      </View>

      <View>
        {/* Tombol login */}
        <Tombol
          type="text"
          title="Login"
          padding={12}
          fontSize={18}
          onPress={() => navigation.navigate('Login')}
        />

        {/* Jarak */}
        <Jarak height={24} />

        {/* Tombol daftar akun */}
        <Tombol
          type="text"
          warna="secondary"
          title="Daftar Akun"
          padding={12}
          fontSize={18}
          onPress={() => navigation.navigate('Register1')}
        />
      </View>
    </ImageBackground>
  );
};

export default GetStarted;

const styles = StyleSheet.create({
  background: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  page: {
    padding: 40,
    justifyContent: 'space-between',
    flex: 1,
  },

  title: {
    fontSize: 28,
    color: 'white',
    marginTop: 300,
    fontFamily: 'Poppins-Bold',
  },
  containerLogin: {
    backgroundColor: '#0BCAD4',
    paddingVertical: 14,
    borderRadius: 50,
    marginBottom: 14,
  },
  containerDaftar: {
    backgroundColor: 'white',
    paddingVertical: 14,
    borderRadius: 50,
  },
  textLogin: {
    color: 'white',
    fontFamily: 'Poppins-Bold',
    fontSize: 18,
    textAlign: 'center',
  },
  textDaftar: {
    color: '#0BCAD4',
    fontFamily: 'Poppins-Bold',
    fontSize: 18,
    textAlign: 'center',
  },
});
