import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';
import { updatePesanan } from '../../actions/PesananAction';
import { colors } from '../../utils';

class Midtrans extends Component {
  // componentDidMount
  componentDidMount() {
    console.log('param : ', this.props.route.params);

    // Jika order_id ada
    if (this.props.route.params.order_id) {
      // updatePesanan
      this.props.dispatch(updatePesanan(this.props.route.params));
    }
  }

  // Functional onMessage
  onMessage = data => {
    if (data.nativeEvent.data === "Selesai") {
      this.props.navigation.replace('HistoryPemesanan');
    }
  };

  render() {
    const { updatePesananLoading } = this.props;
    return (
      <>
        {updatePesananLoading ? (
          <View style={styles.loading}>
            <ActivityIndicator size="large" color={colors.primary} />
          </View>
        ) : (
          <WebView
            source={{ uri: this.props.route.params.url }}
            onMessage={this.onMessage}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  updatePesananResult: state.PaymentReducer.updatePesananResult,
  updatePesananLoading: state.PaymentReducer.updatePesananLoading,
  updatePesananError: state.PaymentReducer.updatePesananError,
});

export default connect(mapStateToProps, null)(Midtrans);

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 50,
  },
});
