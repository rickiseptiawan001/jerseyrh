import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import {
  colors,
  fonts,
  responsiveHeight,
  responsiveWidth,
  heightMobileUI,
  getData,
} from '../../utils';
import { DefaultFoto } from '../../assets';
import { RFValue } from 'react-native-responsive-fontsize';
import { menuProfile } from '../../data';
import { ListMenu } from '../../components';
import SweetAlert from 'react-native-sweet-alert';

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      profile: false,
      menus: menuProfile,
    };
  }

  /* Code ini tujuannya biar ketika klik menu profile maka componentDidMount akan dijalankan
    karna jika tidak menggunakan code dibawah ini maka secara default componentDidMount tidak 
    akan dijalankan yang ada di tab item seperti (Home, Jersey, Profile)*/

  // https://reactnavigation.org/docs/navigation-events/
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      // do something
      this.getUserData();
    });
  }

  // https://reactnavigation.org/docs/navigation-events/
  componentWillUnmount() {
    this._unsubscribe();
  }

  // Function getUserData
  getUserData = () => {
    getData('user').then(res => {
      const data = res;

      // Jika datanya ada
      if (data) {
        this.setState({
          profile: data,
        });
      } else {
        // Tampilkan pesan
        SweetAlert.showAlertWithOptions({
          title: 'Maaf!',
          subTitle: 'Silahkan login terlebih dahulu',
          confirmButtonTitle: 'OK',
          style: 'error',
        });

        /* Jika data user nya tidak ada maka kita tendang ke halaman login, 
                artinya belum login atau belum daftar */
        this.props.navigation.replace('GetStarted');
      }
    });
  };

  render() {
    const { menus, profile } = this.state;
    return (
      <View style={styles.page}>
        <View style={styles.container}>
          <Image
            source={profile.avatar ? { uri: profile.avatar } : DefaultFoto}
            style={styles.foto}
          />
          <View style={styles.profile}>
            <Text style={styles.username}>{profile.nama}</Text>
          </View>

          <ListMenu menus={menus} navigation={this.props.navigation} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  container: {
    backgroundColor: colors.white,
    position: 'absolute',
    bottom: 0,
    height: responsiveHeight(540),
    width: '100%',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
  },
  foto: {
    width: responsiveWidth(150),
    height: responsiveWidth(150),
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: responsiveWidth(-75),
  },
  profile: {
    marginTop: 20,
    alignItems: 'center',
  },
  username: {
    fontFamily: fonts.semiBold,
    color: colors.darkBlue,
    fontSize: RFValue(24, heightMobileUI),
  },
});
