import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { colors, getData } from '../../utils';
import { Inputan, Tombol, Jarak } from '../../components';
import { connect } from 'react-redux';
import SweetAlert from 'react-native-sweet-alert';
import { changePassword } from '../../actions/ProfileAction';

class ChangePassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      newPassword: '',
      newPasswordConfirmation: '',
    };
  }

  // Function onSubmit
  onSubmit = () => {
    // State
    const { password, newPassword, newPasswordConfirmation } = this.state;

    // Validasi password
    if (newPassword !== newPasswordConfirmation) {
      // Pesan error
      SweetAlert.showAlertWithOptions({
        title: 'Error!',
        subTitle: 'Password baru & Konfirmasi Password baru harus sama!',
        confirmButtonTitle: 'OK',
        style: 'error',
      });
      // Jika user sudah mengisi semua form dan lolos validasi jalankan code else if ini
    } else if (password && newPassword && newPasswordConfirmation) {
      // Ambil data email dari local storage
      getData('user').then(res => {
        const parameter = {
          email: res.email,
          password: password,
          newPassword: newPassword,
        };

        this.props.dispatch(changePassword(parameter));
      });
      // Jika form kosong / ada yang tidak diisi
    } else {
      // Pesan error
      SweetAlert.showAlertWithOptions({
        title: 'Maaf!',
        subTitle: 'Form tidak boleh kosong',
        confirmButtonTitle: 'OK',
        style: 'error',
      });
    }
  };

  // Component ini akan dijalankan apabila ada suatu perubahan di halaman ini
  componentDidUpdate(prevProps) {
    const { changePasswordResult } = this.props;

    if (
      changePasswordResult &&
      prevProps.changePasswordResult !== changePasswordResult
    ) {
      // Pesan sukses update
      SweetAlert.showAlertWithOptions({
        title: 'SELAMAT!',
        subTitle: 'Edit password berhasil!',
        confirmButtonTitle: 'OK',
        style: 'success',
      });

      this.props.navigation.replace('MainApp');
    }
  }

  render() {
    const { password, newPassword, newPasswordConfirmation } = this.state;
    const { changePasswordLoading } = this.props;
    return (
      <View style={styles.pages}>
        <View style={styles.form}>
          {/* Password lama */}
          <Inputan
            label="Password Lama"
            placeholder="Masukan password lama"
            onChangeText={password => this.setState({ password })}
            value={password}
            secureTextEntry
          />

          {/* Password baru */}
          <Inputan
            label="Password Baru"
            placeholder="Masukan password baru"
            onChangeText={newPassword => this.setState({ newPassword })}
            value={newPassword}
            secureTextEntry
          />

          {/* Konfirmasi password baru */}
          <Inputan
            label="Konfirmasi Password Baru"
            placeholder="Masukan password baru"
            onChangeText={newPasswordConfirmation =>
              this.setState({ newPasswordConfirmation })
            }
            value={newPasswordConfirmation}
            secureTextEntry
          />

          {/* Jarak */}
          <Jarak height={30} />

          {/* Tombol simpan perubahan */}
          <Tombol
            type="text"
            title="Simpan Perubahan"
            fontSize={18}
            padding={10}
            onPress={() => this.onSubmit()}
            loading={changePasswordLoading}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  changePasswordLoading: state.ProfileReducer.changePasswordLoading,
  changePasswordResult: state.ProfileReducer.changePasswordResult,
  changePasswordError: state.ProfileReducer.changePasswordError,
});

export default connect(mapStateToProps, null)(ChangePassword);

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
  },
  form: {
    padding: 30,
    marginTop: -20,
  },
});
